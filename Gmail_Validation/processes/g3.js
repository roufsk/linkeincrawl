var request = require('request');


var async = require('async');
var mongodb = require('mongodb');
var config = require('./config.json')
var url = "mongodb://" + config.username + ":" + config.password + "@" + config.ip + ":" + config.port + "/" + config.db;
var proc = "g3";
var server = config.data_collection;
var collection = config.data_collection + "_" + proc;

mongodb.connect(url, function(err, db) {
	// db.admin().authenticate("tpuser", "6Cwm7s.5Dk", function (err, resl) {
	if (err)
		console.log(err);
	else {
		var mail = db.collection(collection);
		var data1 = db.collection(config.data_collection);
		var server_stats = db.collection(config.serverwise_stats_collection);
		var stat = db.collection(config.processwise_stats_collection);
		var processes = db.collection(config.processes_collection);
		var unvalidated_mails = db.collection("unvalidated_mails");



		function id_daywiseStats(incDoc, source) {
			var mhd = getMHD(new Date());

			stat.update({
				day: mhd.d,
				source: source
			}, {
				$inc: incDoc,
				$set: {
					updated_on: parseInt(Date.now() / 1000)
				}
			}, {
				upsert: true
			}, function(err, ok) {
				if (err) {
					// cb(err);
					console.error(err.stack);
					process.exit(0);
				} else {
					// cb(null, "ok");
				}
			});
		}

		function serverwiseStats(incDoc, source, server) {
			var mhd = getMHD(new Date());

			server_stats.update({
				day: mhd.d,
				source: source,
				server: server
			}, {
				$inc: incDoc,
				$set: {
					updated_on: parseInt(Date.now() / 1000)
				}
			}, {
				upsert: true
			}, function(err, ok) {
				if (err) {
					// cb(err);
					console.error(err.stack);
					process.exit(0);
				} else {
					// cb(null, "ok");
				}
			});
		}

		function getMHD(day) {
			var dd = day.getDate();
			var MM = day.getMonth();
			var hh = day.getHours();
			var mm = day.getMinutes();

			var yyyy = day.getFullYear();
			if (dd < 10) {
				dd = '0' + dd;
			}
			if (MM < 10) {
				MM = '0' + MM;
			}
			day = new Date(yyyy, MM, dd).getTime() / 1000;
			hour = new Date(yyyy, MM, dd, hh).getTime() / 1000;
			minute = new Date(yyyy, MM, dd, hh, mm).getTime() / 1000;
			return {
				m: minute,
				h: hour,
				d: day
			};
		}


		var headers = {
			'authority': 'accounts.google.com',
			'content-length': '10659',
			'x-same-domain': '1',
			'origin': 'https://accounts.google.com',
			'google-accounts-xsrf': '1',
			'user-agent': 'Mozilla/5.0 (Linux; Android 5.1; P5 mini Build/LMY47I; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/58.0.3029.83 Mobile Safari/537.36 MinuteMaid',
			'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
			'accept': '*/*',
			'referer': 'https://accounts.google.com/embedded/setup/android/identifier?source=com.google.android.talk&xoauth_display_name=Android%20Phone&canFrp=1&canSk=1&lang=en&langCountry=en_us&hl=en-US&cc=in&flowName=EmbeddedSetupAndroid',
			// 'accept-encoding': 'gzip, deflate',
			'accept-language': 'en-US',
			'cookie': 'GEM=CgptaW51dGVtYWlkELCal-HkKw==',
			'x-requested-with': 'com.google.android.gms'
		};

		function rotate() {
			mail.find({}).limit(1).toArray(function(err, docs) {
				if (err)
					console.log(err);
				else {
					if (docs.length > 0) {
						async.each(docs, function(res, cb) {
								// console.log('*******************************', res);

								var gmail = res._id.toLowerCase().trim();


								function g_validation(mail, cb) {
									console.log(mail);
									if (mail.split("@")[0].length > 5) {
										var dataString = 'continue=https%3A%2F%2Faccounts.google.com%2Fo%2Fandroid%2Fauth%3Flang%3Den%26cc%26langCountry%3Den_%26xoauth_display_name%3DAndroid%2BDevice%26source%3Dandroid%26tmpl%3Dnew_account%26return_user_id%3Dtrue&f.req=%5B%22' + mail + '%22%2C%22AEThLlzwiPtHb7oK5ezcd4bN245dbV-K3Uu9WFUdvRprrekMsrS7AnKbgaN-CORRBLzTw7mN1RcrFjOcHo40GDQlkeeoSyrxu_kV6o39jiI64xp3VJDICoRQHjX5k8MoeToSCKMzD3Gs%22%2C%5B%22anonymous.requests777%40gmail.com%22%5D%2Cnull%2C%22IN%22%2Cnull%2Cnull%2C2%2Ctrue%2Ctrue%2C%5Bnull%2Cnull%2C%5B1%2C1%2C0%2C1%2C%22https%3A%2F%2Faccounts.google.com%2FEmbeddedSetup%3Fsource%3Dcom.google.android.talk%26xoauth_display_name%3DAndroid%2BPhone%26canFrp%3D1%26canSk%3D1%26lang%3Den%26langCountry%3Den_us%26hl%3Den-US%26cc%3Din%22%2Cnull%2C%5B%5D%2C3%5D%2C2%2C%5B0%2Cnull%2C%5B%5D%5D%2Cnull%2Cnull%2Cnull%2Cfalse%5D%2C%22anon%40gmail.com%22%5D&bgRequest=%5B%22identifier%22%2C%22!GBulGzpChCR7PoO0we9Eb1thea_jxFICAAACuFIAAACJCgAL7q6c0uO-gbhRHL6ZARUB0kOCBtoQcns0Rz99bwManQ-cAmBqUttM_a9rcJ2a70uv-OYjyK7eNCafZ3L32wBiVC8QOfwvC_wy63jxJ-Henp_8k85ez0f6vSCIyxEjPiviI0b2R6ZKc4eywMaFQFDE3V2jnxITFMWn0UepCEuEO0qvO3JmhyvZ4T7itdUBf3Th7bT38Un4Bo_JUCjHzVSDGkLyY-A4oBdg1Der-a4wdPIiUnZJWtd0NUcYrzRvd-xzxZk8H3qNLwYZw35OX-zG1HB1WmtMiwkjWnyzOPKo4ZhyTY_bg1B3sFw8hgm3z3r7G9LP6R0dGjzndGNUW4voMvbo98EdoalKOnFdgPhjVbBWq6pn_fMu64W1KCTd0NHWHRmq%22%5D&azt=AFoagUXIRxPpQ4aKqV-UAbYt-96uUW3ldg%3A1504516099378&dgresponse=%5B%22CgY0De-ULIrYAQHaB4kEAOuhCBKiY9xLvfIf9rFZ6LGvulWSGoGx6oCiWVmVQ0h9RONy3LzwJZV6lXEjVXCst8kSjDg5rhYtCckSm1cIf5vq1GTzgUJiSHmQNNeLZevT4-EEyqx7LpAl_r6mrkulnjgGj1X4psFVuswG1oLdAw-D5xN03rJwaOHzFpqCwI1vHK6f8KuvURat9f3Xhr6bzpagy-Sr7YZlLqYUgFnmMxev3_j5640nYh6EjwJzORntPaG6CqdfImv_vRol4qtiylyeqPGgesFhegJUzuBUaZJgeH5z70RDI0iGurNcT9YD5mixg9aQbs7nqlsKrU6sRASkMrqC9yhfBiOtSdMfqKzpbqt1C4PCp_IhjbMag0eurJKRfqw7-wLwgFUOBKNCvjb5GiBPRXlAcUOSDUcKododh1-7D4AUgt_LDtLxsAQa38GTS63flZFnTBmI166MaWgrEPBajonOtsjaR6uax09n_HzoldF0q7NLIC_TLcjWo1f4VqsegN5BTRnsh1iruMldMVhD2urSQy3Mv0JFBcHBf72_KI5gCoXhMkdrPT4LuVvMvp6WZzNu5LHOcjAUbEaNXnJRArTQ9BaBLw0v7gqvkiZRl8O4dFwnV_8OtnPZKnPKzDp5uHOZ58_rwdyvc5PH8kiLO9q8Tc_4ZOAVjOEMlj8Mr5RIm42GQYbZv4MDHr85mdrTDI84YRoRCMP_gPj9_____wEQpPDN2wXoAd6gj6f7_____wHoAeav7rECOGs4SxKmMOJV8xWBoOVVwoN_5kjhB2INN3xWKsCcsqNaw0ArzMOdYRV9gMa9OoRUinIB5eyo31HQ34kzZs2SrYXP_LZi2IHBaVIq0gAYW5WNwkhQipYOGG-1O8YUPvVHl1VZAYqD399PQGhVQwp2UWOglC-WmxDsb601abi1u5NfMPnsECOfvK9bgjDoXvlF2RQ1wXdgbXb2vJiyXrbmugM9coA3k_z6WmcJ0KX6tdL-w94Ki4rOupDOJKVQA5Ph28yCTtYAY24OOk-q47nGALlwj2fQqXngAcl54Vn8jWYpT2lytCqGsmPAInQDEyx55gv7HPm0KvFW3SaWyLYtG7ZAD3SMZc3MZEpCUa5kGgBNuOKa0w1H-QxzVuk5s5eGaPpjgs_61_H7T1KhmN6-E4tY1c9pxpAFYDCwEiXOA4JPLxnrViDtl19aEYMvT1jCutaAJwoSpHN5sq626v8ex3Z0HvapTaFOPY7ElEHH53fynx0kQmvJu7r8hpmgJpRhaTlz7bGeCnOR30gVtQaUwyPy3bRtHA7qg_GO14tcR0QeVWFdGH5MP65G5HTT8jnbOff4igMZElrPT0GAfLFFnB6ijwTyEasuA5KrIbTNoWF_dhDeeY-LCL3fFxxfLQRxGNbrld17Wml0VWrnled1h4-rOOG0_2wRWSLtApxGXMphj6c-_0fQYDvH41gO6Y_IHxLIydOWJb4Dz-iym7BQiPqhaSEnsZJU_yqzjyAIUFpB-_RAa_3EBpIiHf0mVEFDCL9qVd14e985l76ssI0Qgda1F2nkt8vRuEn-sRbxAdciQR1vjAIxuxxflJJ7W1bN8rQ-Pi3OM6MiTUpvb3jqOBbZw10AEF6fzc5pmkYDPByD9dksA19Ixg4cgJZ50beEE4UH2s6Ls6iVpH6Z_bLSblPR7EGi2lXF6Za6zzYeA--ndm169f263S7L7mOimSf-ifHhzRbMGRsswjTBe6u-BuoN5F99DrN-umanZGZ-4LDSGcYv7IM9seLqtgyPZbtvrHonWXe6oOvvM76P0JbncHacfX0-73lCDn4L3-IlLEvnP4ypsVtZJJUBvpRVit0RKnxMfwfa_aWRz0Dv8RoKE2HBNiY10Vxgp2M5Kc9e3epUb9ev1Wlv-YB6qmtSmSavvNvFKW3dBYzfrOADh49N6mR4bGgwcCh5O98Qzd-a0t0vdxTYY7H86QCkgr0gpRcyKKGlApCIQto6v1JpeCdgvTMsl4fQ5mkH4Cm5U49AFmJFflZsG5GquVlpCzlqrmIMkQAr1Bv6XQm2FnUJPPCbrfIC2rrcYJowVEZ4N3jvO0RnhdP3Vi7YlweaUlagDd1QG1Y_Kw8JlwgfNByfpTvozBrRFSRAT4_2L7Ccr_fR455hW-7ijXtK-LnMKLQeKL_K2bfFy6D24Mw2ahPvOFoAhxonJfTx6tffbPrVTZFQ82EUUCXNG2-dAoXcVI25ZiQxZo0cVnsFZ3UE7QDVG5_lz-bJgoCBfBrn3W7ZgMNMibnDhG4SizR3xcRA8IruO1GpSXXBSLvgKdVTdH8L9McbXInyF-gZLRWwzD3tkv_cNQh_g3DhRsvDQP-wkTinozoP8Iybs7tWq5_Pef4Imu8XTjKO24tiLnhzs8MFIT8rjAkAzJUM3-DFdZbPA8ly2qbMOlTfdLeiwM3KSbisT0bVqVL-2-lP-I_-Lb09etGlSxfXSfiQVlEooTAV-R4SAuEkZqv7qoMjNXn5UQj86gDfLdeuSBxs7vd83bHc-Z7SAd1qFOFWYlytG3NlE8IG_Z4HYZe82qQRdQKYrL2XoP-z2KoomucH73NrPUfxa8xVI5KWlrHv_SomsqbEUN6inBVWrAsmxUS4YHBrk9LnseE9HNZlsy9cfQzHNQRz_EmK91g5l4C_Lu27fUqDv-J9PtiMpzjX7XoQN3reRK_Gn3NbNBUQS4e7vyDyu7jI9JHcAkXVBBhO2_6M75RGbMIAjI82o_hfAcBUdIXBJEDkA5TOhAKCyY8upKsgj1GBDIoEcJil8GNeO7vVDUgkxlBebZm5q-Oa6ER95eZG6vrRgycvjHQgFaM4IChRyYZh_-KFG1tLBTdYtyNnOkmkAaVL567chtr_1QnId1bS3mSTXe7gWbDlAlCuG6Ze4IQpocK55BA-TD8GFfZuGy5nCyWiDfG6PJHYmK0KuW2SWPLRcDRpkHk6h4Hnoyhpba7Ll4rKJu8DZW7OIeczvyosSUg-eGYHBhzSrcHJYcdETCRbAnSI9Tqg3TZqxtEZ8_hy_KrX6dZ0lYuhjvYX8ioO_isF9xLQyrabHxezhQ_0Lpgd6UvGhz-_5k0r6Gk09WhhE7KOVsphhvYvYOl2L0ZgHGymBHxyVjt1CBlpcgapj3AOu5ezcB7sgkH4-5Kq1AVY2KUhgyEkpumK_lO_wyNxGp-sP0Sb20hWnUzAMvz7IlqnyBHVsDryAAEGd9tgxzJqLFv3hPnX6NkB7zbZ9M5D7SCI3EtzYP57YROXm1wFdr63PoTRrgS-rPVn7GYizgSPhfFWScqBLiHj0qwJmzfTTUfp59_I0_2ohz-sS_43e8hZRCBguHolGoUZfssmquBfh0D0SOZjOdJl6kj_njxVxAlFQKPYrFjeHMihSBNaP0--RyfKms1a_o9OtTdihQ1YXD1zCAnJQcFSuzIQSCykP_DANmE4nsj219wHJAlnVslEdQ6nXJNQTqUfbOqiHSp6qb4aMxCepwMPibH41wSj_hmuDnKhwat4uYQxOg06KdM44iCWwTFAA_0oDO5hvwBuMjJmApiUyw6J8qlql2As3BmLNEonYKIeRyVHDXP8Im6-I8uGKtrTXx5IatuL94vVB426Z6IAJSV9Xv0KidWj85mBR2tvRHPvBilgCeRlSyp4Y98uaNvuePXrFlQArJJcby4t15snrmIhdisjDfLIHOzkCIOohOugsPyH0yqGms0kC9KLL6mbkeWXexly5zGOJWhDXnZ6h0r82u3bXe3-pfNFKcErGV7YSQ2hEqNvEy-TQFLeTfw3sIqK8NrITY4rvh5CSSGemCNqls7jZGpKozPSexuEip41gkIm39zEbDNmbjnlxnGDoHgFtimi3CrraK9o30TL3KwcE3yJ3sIpAJm7Ae5_cY4tKnR1DJadH7QcJUEfs984Kim_Pb0nO8W5NjYeMlIODXKRgIlxHQo7nxgCS4hseSvHtxy6pKw4PUIdnQ9Ps4bAuPC-tGLJeot8wRF7zZc6nPBnGMB1AB89Er0gInWGX_CH1BTqEOp2njZYqcR99mSJDelVehTbKsErqhZbC8-0sFMpsQVXlEEQCWf64NhyhThhQu9OfCmQZQ4CKVTMi0bSanJozv7Xc-KZbXFvoSJE1qSKICCp2NHKmxyOriY335Drau_uDS60otPaZ4KEXL4G2T87OjKcE-NJyLwkIumtHMUlDgDCoQTfCM-95RoUss13C2msEhIwWLAbmzO8oYqt8pQTKFGldSJzXKXmNIX07UR9imwPloscNhG_Rexe3hd_f236VbN3HQutSJhdoqhTIh6l-OzO38WwF-oexYy46Fi1c5erHcAFYLJPxpRhFpnL97M_Nk8RnsrYvOIaInkeNxYkfEWQXNMzxAKuwU_8DqAF6Tu-f9hCSMuPpJ_L7lnd4zHlPZofdY1vGLKsu4-2PobYXDvwAdkGy5hZZSe967ioFCewExo47CWLru8Jw2thNkyBAJ-Gljcdw0vSZHyGS7PdObqZpkUZsQPHY7vz8k6lWjYUrfj2WhfWJHHY9p33XbPsDoHCToijqT9EVDIdYmzYa1zWB-kCwPvKD1O732x4DIASHpu9RxNTFDol_GTBv8NwZV8jmLFA1_YE2BAJi87gJ_c_VsuDB6MADvMQR9Waix1dQd7NuXmy0lIC7cBRU8lTqD47JiiLHC3C7r4i5kBhh96hxENE2PlcDc8lY-GOvw5keSzIebwnzmDaJOKPIQYm66HUZseLVBKBu0CSL49f8yA2BoSyZPD8LjbvcWDJLYIyw4jhyuyk7-rVRCh2bUV8JH8lO0vCYjsKLDYoFCM_a_4IZwBmqTskM97BzlOKxp2SWUNTryEjzMlQwCe7dZCl9G_JGDZ9HlUJHDGVZ6X1MVRaANehDBlsItECMRSJ7AtzE4gyoLLy9c05cDZ2mkCxF4FzPgeiUXVJka-84sxtWM7mYDgeJIDBn6CXlpE6PCDTB56Soe4sk0e2WB5DZPwoxasB0tkSbul30mQw89zY6-pm67mInw2YyTL1R6ng71VodgABpyfxtGi7YClWQiZDJ0iYoqN10HB8qCXxPOXTiwRdlGB2xEEoqMQIEHBqbknrHrW1aJLO_KcfHZ8M5IaqGT41wKb63qTWoJLlRAv1GWkwsDtjYw41k3l3Mq1wyglXVIFunKtzm2AQRrSi7Cy0bOsLfdKDAUZAisajhBtQqs6hC33fD3HoZIC2wFfD7Z7gMjYGb0I811JVikBc4rp44SxPU0XKmhsXA8edAZFlPCAtK_HUD-z5RvpX89vDEvf-2wCWOIvZNZyUuqaGt2lZepFGKiApkGXuj9DmQ4KqX-_3kQsB3UFf1rwyNnY8KniPx4SUjBrEZjK8USYG3Fzjjk1OqvNvgUtN6EpS5atQJE9UCK917zYMckex8RaszS2c8NI8RZCX0hOzTgybTQiPY5ggjmqNOAYu99BzZdM5zukxsiy_xsSXfNoVfuMtsaGF_pvUvMzPPfvSHnePsBgDXm6dIbfhmsFw_D45eQ9GlGL49Ev0hHw--rPhRQDNDqfP9cgcjAEnbkEuvm_nnqd4ZHeSpO3rGGxdZNhAMOHHmRaUU_sotKQH8AL1wj529C1Xm1bBTBGPyXkXuEEOqlDfxoAsR72QfjlQbD_Y4_uk0OwrEoHAPf5vs2prWp7WYpfezcBsP0OQN_gWpiDMbzZVeKO3XXp7PuHzkA-VaIu7BvlAY6v1a_O2zGyhweGyR5F7yFEOlqR7HKK34kJKyorw5ItN9lEmH1_N11cVdlzYYnMESan3oew3qUny0tDHKasQK2WLppknvKJmZVkCiclKix6Z0ydQ9OqVCd8fV7Up_mUP-cFVGbT3w7egcRP_4iPgq3izEJkAA-96iztBR5ebY9ASVX0jmOLu39xWlAU7Hv6IZi7dJc4EhxISFtqxzYCwR0MrYygUxVY8Q-CYfOdA9OcoGjJmi7tWO_b3b6XqAZ-jueHDUUKNGyvdlio65TACNNFL175-WSjJ30ZV9wbJyKXUtprnbzjK6iHXyM1VhcZBligLZ7uP49qbWXOwLb1ZhhlSd4gnG-Ib9gVfGEVKtcWmsFgpmJeojBrOqB69WXrbDgz0vTx_LjjDnICnCV300Ev27owZ7GVjkwS7B-PIehR60Z0PdLes8gxSxXLaYHGFvSak7aoysX_EJMjfQaVtk0UHb4_Px_NhMyBlnttxu5Cm8lww5eoT4Zh9j_6hdCy4OlTfLaqg_1RPgTCHblqWhHHmpe5FyNLjKFh4IsKMR1WMjlVrstuoU_qnUBIRzNr7DNxka0xwYqrmr_8Q74dfjp-n0u_bVVl9XL993kWNPp968bZlJ1vQHeXhzhXxukUpDb_u91KbhomSqrbruXI1pStlOIg-IUqhUHapOG_QGpSrotOldNwiOtfG7IL3e9QLnQP947zEIJI1kP_cM3auKIwi36BOZxaTRmfyaqYhiNjel_bmmHUOfspMDjwLnL3MVZpOND34dcNw513urzZnHntbVnQfv-lZWJvdIQn3I0R9P5yO9mvuAyAmA9q2yMlLUQiG7mYU76l3sqL9ln2nZasZQLHSW0TL_NbpWDKSMxMurQBREDAnKWk-C1AIChSmRxDQLvQ-FEoLMEL2onVxNWsXGR-GnAZrR4vZFfv_LeqJHZ40W2ODH6hGvS_yTtnA3ijgO4vrLMpUPzD8Zy3bvuoVygkcNkt5XRDMVZNDqH1rU5QVlLjKt1qWzH-G0fKA-lPWqcq6pJsQkzMflIZ4z9X_wiFmIn0lGddV_JniKp3AdT26Ti3KkxE0pt9KAXvEoJZLLw_DSx7bV8tECWh9tGBHGbdnkTNyx88mmDQF2aIMzOIHjpnKzVfBBp1XsGkOgYUrDpR2QzfUCHyMaQSsWoXB6UccoZUYxlLrrlCdHHfVT-fUMAapoRfRLytL3J9HnaU4Y7Wg-ZeslyBK1eSbGle5unM24DEJb7n6EeVTUvx2s8ay4i9qUrMlc9hWlh52JuHySoSP97ZwP9y8u0UoFFKci4zY5xEdL_M7t2r5tRMuVnEE06fidPL_TzAhLvCuezcwhUmI2oFL9JFE4Z2fHhH1B2nwmKg1pM0alkcvzKGTzVpkB8s4oBXvCB-uThAQF52OlIISLazLMxxyWTIerWHX4eqCAYAbQFrjOV4ZSnZXcORJYIGahVwRno7RhWDieHJw6CnwaGYnP4hGIr-x0j192dbCJ6eC5jRG98Svib1lqj78ySl0qkGSF0T34JuHwyS35Z76LcVd2z14BkqEu7-IQZj7MqdFBkjwj-vlIMX5kiap2YHH1_g1_cmlsEw0sFF83Fq0P21AlWJPvLMRVGwblTG8IiILMt5xLJmgi1j4hGW5J17zkqGH-kJKlViEIzd-gSggqEiVrl0yJbRgBQteM2P4bwIPQ54PlxXKI2VJD0WTRlqoTd9Zq_-ckp2k293okDNe1WpGr_S7Q5nX1kNWHXLGhm_3MXficxrSIjfu80hBtRgwFDXzh675wQBi2Qy8bw5IMsfT6HBRuXXZh6fLySVf_J9E0bWnMBD7JOkuE0ZzSDnIrVwTVAZf_ejNfAS5YnG04ZahZ1QZfdhcHTc0otaJiZi7pFq5QiG-IaoZ754QTa4sPObSUZgsdmNZQDR7As-OBi8XWp57rJz0bvjFNrp_Q7AAIFiv88gQNdIarq4bZ-8mAbr-8Es7DIgztVNQstyZupBILGnO8YpoQkGok0P1pPLViX4oOpgo_h10m_NgXDA5F35xmHPOcOXhel9ebyZUizPDwF9Lsqff92IO06BXjP1gUMCgKUsZOJP0pmNtDqyqpaJ0mPDu4EtP9rA50YMI5v6KGZ6ruYHzgMo6__QF3clS4nYtd4-Slwqo9ZdgIjyJ7kElAcLaJYFtoCxsxHvj4PMo2v9GoCMC7sRcOqTCa91MY_82GDGDACrhJqhKoORVjw1Fhy3-qq2GCxTw5mJZG1BwZSdk8IRfD-cZW6XY1VFdKa7HzJtnBzjhamZaOpHw5j6s1k231I9_4QIcFVohc7sUMsXkezwFbEQjhVMFzvIkN1ns2KbYgIFTK6rMMTxv5PfKbEeAi4uG7OmB3pQifymZO-c_T5EkAteqXS-TFs-toqpkbNU4bpyoljDIRmmhECsTjtVmEatQcT23iaP7W2qnc-JvE0mLBWUDZFnp9zQfgiBEoB2lWtQng8Reeua55oOVaxDJE-BK9x36epceSYStyZyIK5os1rj0zJ2At-0O9UN-WeWQJVHoIph_U2R0J_wLJLZansNTVTJ5DLZjb-SR8yxcaqNw9uRLrtWxoRGHqJeCzjJ83xaPy0buS42SAdJqmj0Y3vD_5BwvzIEYm1ltP_0yx2xnjWHOmFgtqjdrzFcen9CVIyYSkqcMWv2pMgrVeBC2NvCXC2VP-1A6u6590820lMYFL29W-02eWQTfk0CH0pX1jCGdJPEGJMlpSBfaMHPgeSsK7jCiarFz2rbfmKVtFhNhlB8uODh9KMImt-UgSE7O-3g2vnaAKiBELK6MrdHImaS2h_zj2PVYFwPLTyHFx2u7c-mp3CeRQrLxYvc2l33WL1c7VX2_wp83_eG72ZzSwB9VNvNojVrng58TF_cnUAXs_uP_kDWXXKpZrVryqn4YLvljLGf2O-WBu_hVpKa3GWGErPSIvfEUKPAz4vObLX1Ah32uhR-VsbsbQ0cYI0T9L64FXkMmGwzw-xPv8lUJhN2rhTiNjTb_YIFCtXAfY7rL1NXeleD233SALw0nzB-SPkqHl9Ygj_vVCI5LxXXf7jRvlDHYEXAkJ3O4TUkjItkgjaKuS8NNHuXRB4SDprL8Dm03rROuuqvkwUSKeoGJRxPDLRRjpAfb7jFPkQufZPwvn9vEb91e8xcqS_WPiDoZyfjD0Q5q6ybUlcw9hX0ARHmh3bIQz4yPvvxWb2ANh54XV58pB7OdjhQAROOloXt_843YGPmPmQe9vD5zOW-bGb5j3EzA01oDbNnQ6S6_9yNnLARrKo6P%22%5D&deviceinfo=%5B%223e38d3b337ce3e79%22%2C22%2C11509234%2C%5B%5D%2Ctrue%2C%22IN%22%2C0%2Cnull%2C%5B%5D%2C%22EmbeddedSetupAndroid%22%2Cnull%2C%5B0%2Cnull%2C%5B%5D%2Cnull%2Cnull%2C%223e38d3b337ce3e79%22%5D%2C1%5D&gmscoreversion=11509234&';
										var options = {
											url: 'https://accounts.google.com/_/lookup/accountlookup?hl=en&_reqid=52711&rt=j',
											method: 'POST',
											headers: headers,
											body: dataString
										};

										function callback(error, response, body) {
											if (!error && response.statusCode == 200) {
												// console.log(body);
												if (body.indexOf(mail) > -1) {
													id_daywiseStats({
														valid: 1,
														total: 1
													}, proc);
													serverwiseStats({
														valid: 1,
														total: 1
													}, proc, server);
													cb({
														email: mail,
														valid: true
													})
												} else {
													id_daywiseStats({
														invalid: 1,
														total: 1
													}, proc);
													serverwiseStats({
														invalid: 1,
														total: 1
													}, proc, server);
													cb({
														email: mail,
														valid: false
													})
												}
											} else {
												console.log("error--------invalid cookies  or token")
												unvalidated_mails.insertOne({
													_id: res._id
												}, function(err, ok) {
													cb({
														email: mail,
														valid: false
													})
												})
											}
										}

										request(options, callback);
									} else {
										cb({
											email: mail,
											valid: false
										})
									}
								}

								g_validation(gmail, function(data) {
									if (data) {
										data1.update({
											_id: res._id
										}, {
											$set: {
												valid: data.valid,
												g3: getMHD(new Date()).d,
												updated_on: getMHD(new Date()).d,
												validated_by: proc
											},
											$unset: {
												lock: ''
											}

										}, function(err, ree) {
											if (err)
												console.log(err);
											else {
												var incmt_doc = {
													total: 1
												};
												if (data.valid) {
													incmt_doc['valid'] = 1;
												} else {
													incmt_doc['invalid'] = 1;
												}
												processes.updateOne({
													process: proc,
													server: server
												}, {
													$inc: incmt_doc
												})
												mail.remove({
													_id: res._id
												}, function(err, jkl) {
													if (err)
														console.log(err);
													else
														cb();
												});
											}
										});
										console.log(data);
									}
								})


							},
							function(err) {
								if (err) {
									console.log(err);
									// process.exit(0);
								} else {
									console.log('loop completed');
									setTimeout(rotate, 0);
								}
							})

					} else {
						console.log('db completed and retrying');
						setTimeout(rotate, 0);
					}

				}
			});
		}

		rotate();

	}
})