var request = require('request');
var async = require('async');
var mongodb = require('mongodb');
var config = require('./config.json')
var url = "mongodb://" + config.username + ":" + config.password + "@" + config.ip + ":" + config.port + "/" + config.db;
var proc = "g4";
var server = config.data_collection;
var collection = config.data_collection + "_" + proc;
var logger = require('tracer').console({
    format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
    dateformat: "dd-mm-yyyy HH:MM:ss TT"
});
var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');
function start_DB(host, db, username, password, cb) {
    var url = "mongodb://" + host + "/" + db;
    MongoClient.connect(url, function (err, db_conn) {
        if (err) {
            cb(err);
        }
        else {
            if (username, password) {
                db_conn.admin().authenticate(username, password, function (err, res) {
                    if (err) {
                        console.error(err.stack);
                        process.exit(0);
                    } else {
                        db = db_conn;
                        console.log("got connection");
                        assert.equal(null, err);
                        console.log("Connected correctly to server");
                        cb(null, db);
                    }
                });
            } else {
                console.log("Connected correctly to server");
                cb(null, db_conn);
            }

        }
    });
}
var headers = {
    'Host': 'drive.google.com',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0',
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    'X-Same-Domain': 'explorer',
   // 'Referer': 'https://drive.google.com/sharing/share?id=1hirbKi-wZMBadnvym5EG_dCWOu0KusnwQLG9Mr0UOIY&foreignService=explorer&gaiaService=wise&shareService=explorer&command=init_share&subapp=10&popupWindowsEnabled=true&shareUiType=default&hl=en-US&authuser=0&rand=1508313097942&locationHint=myDriveItem&preload=false',
    'Connection': 'keep-alive',
    'Cookie': 'SID=TAV8ZQ5awQS7DZlPCgENpbuRNXqIK1E45s0pvh_syIzP3iVMoS7U4Q-fWMEMVz7hmj5Swg.; HSID=AoiIObJM2oEiOtYDt; SSID=ANHIjIX496DQ8IHPc;'
};
start_DB("103.18.248.33:59765", "GTA", "tpuser", "6Cwm7s.5Dk", function (err, GTA) {
    var accounts = GTA.collection("accounts");
    mongodb.connect(url, function (err, db) {
        if (err)
            logger.log(err);
        else {
            var tc_mail = db.collection(collection);
            var data1 = db.collection(config.data_collection);
            var server_stats = db.collection(config.serverwise_stats_collection);
            var stat = db.collection(config.processwise_stats_collection);
            var processes = db.collection(config.processes_collection);
            var unvalidated_mails = db.collection("unvalidated_mails");
    function token_rotation() {
        accounts.findAndModify({
            server: server,
            process: proc,
            active: true,
            use: null
        }, [], {
            $set: {
                use: 1
            }
        }, function (err, res) {
            if(!err ) {
            var doc = res.value;
            if (doc) {
                var account_mail=doc.mail;
                headers.cookie = doc.cookie;
                function mail_wiseStats(incDoc, mail,process) {
                    var mhd = getMHD(new Date());

                    accounts.update({
                        mail: mail,
                        process: process
                    }, {
                        $inc: incDoc,
                        $set: {
                            updated_on: parseInt(Date.now() / 1000)
                        }
                    }, {
                        upsert: true
                    }, function (err, ok) {
                        if (err) {
                            // cb(err);
                            console.error(err.stack);
                            process.exit(0);
                        } else {
                            // cb(null, "ok");
                        }
                    });
                }
            function id_daywiseStats(incDoc, source) {
                var mhd = getMHD(new Date());

                stat.update({
                    day: mhd.d,
                    source: source
                }, {
                    $inc: incDoc,
                    $set: {
                        updated_on: parseInt(Date.now() / 1000)
                    }
                }, {
                    upsert: true
                }, function (err, ok) {
                    if (err) {
                        // cb(err);
                        console.error(err.stack);
                        process.exit(0);
                    } else {
                        // cb(null, "ok");
                    }
                });
            }

            function serverwiseStats(incDoc, source, server) {
                var mhd = getMHD(new Date());

                server_stats.update({
                    day: mhd.d,
                    source: source,
                    server: server
                }, {
                    $inc: incDoc,
                    $set: {
                        updated_on: parseInt(Date.now() / 1000)
                    }
                }, {
                    upsert: true
                }, function (err, ok) {
                    if (err) {
                        // cb(err);
                        console.error(err.stack);
                        process.exit(0);
                    } else {
                        // cb(null, "ok");
                    }
                });
            }

            function getMHD(day) {
                var dd = day.getDate();
                var MM = day.getMonth();
                var hh = day.getHours();
                var mm = day.getMinutes();

                var yyyy = day.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (MM < 10) {
                    MM = '0' + MM;
                }
                day = new Date(yyyy, MM, dd).getTime() / 1000;
                hour = new Date(yyyy, MM, dd, hh).getTime() / 1000;
                minute = new Date(yyyy, MM, dd, hh, mm).getTime() / 1000;
                return {
                    m: minute,
                    h: hour,
                    d: day
                };
            }


            function test() {
                tc_mail.find({}).limit(1).toArray(function (err, docs) {
                    if (err)
                        console.log(err);
                    else {
                        if (docs.length > 0) {
                            var test_gmail = docs[0]._id.toLowerCase().trim();
                            g_validation(test_gmail, function (err,data) {
                                if (!err && data) {
                                    data1.update({
                                        _id: docs[0]._id
                                    }, {
                                        $set: {
                                            valid: data.valid,
                                            g4: getMHD(new Date()).d,
                                            updated_on: getMHD(new Date()).d,
                                            validated_by: proc
                                        },
                                        $unset: {
                                            lock: ''
                                        }

                                    }, function (err, ree) {
                                        if (err)
                                            console.log(err);
                                        else {
                                            var incmt_doc = {
                                                total: 1
                                            };
                                            if (data.valid) {
                                                incmt_doc['valid'] = 1;
                                            } else {
                                                incmt_doc['invalid'] = 1;
                                            }
                                            processes.updateOne({
                                                process: proc,
                                                server: server
                                            }, {
                                                $inc: incmt_doc
                                            })
                                            tc_mail.remove({
                                                _id: docs[0]._id
                                            }, function (err, jkl) {
                                                if (err)
                                                    console.log(err);
                                                else {
                                                    //cb();
                                                }
                                            });
                                        }
                                    });
                                    logger.log(data, "test passed");
                                    setTimeout(rotate, 0);
                                } else {
                                    if(err=='token expired'){
                                        accounts.updateOne({token:doc.token},{$set:{active:false}})
                                    }
                                    logger.log(test_gmail, "test failed");
                                    setTimeout(token_rotation, 1000);
                                }
                            })
                        } else {
                            logger.log("no records");
                            setTimeout(test, 1000);
                        }
                    }
                });
            }

            function g_validation(mail, cb) {
                var dataString = 'hl=en_US&token='+doc.token+'&foreignService=explorer&shareService=explorer&authuser=0&locale=en_US&requestType=profileInfo&confirmed=false&emailAddress='+mail;

                var options = {
                    url: 'https://drive.google.com/sharing/profile',
                    method: 'POST',
                    headers: headers,
                    body: dataString
                };

                function callback(error, response, body) {
                    if (body.indexOf('The requested URL was not found on this server') > -1) {
                        logger.log('The requested URL was not found on this server', m_count);
                        m_count = 0;
                        cb("change token");
                    }
                    else if (response.statusCode == 429) {
                        logger.log('The requested URL was not found on this server', m_count);
                        m_count = 0;
                        cb("change token");

                    } else if (response.statusCode == 403) {
                        logger.log('token expired', m_count);
                        m_count = 0;
                        cb("token expired");

                    } else if (!error && response.statusCode == 200) {
                        m_count++;
                        logger.log(body, m_count);
                        body = JSON.parse(body)
                        if (body.profileInfo && body.profileInfo.id) {
                            id_daywiseStats({
                                valid: 1,
                                total: 1
                            }, proc);
                            mail_wiseStats({
                                valid: 1,
                                total: 1
                            },account_mail, proc);
                            serverwiseStats({
                                valid: 1,
                                total: 1
                            }, proc, server);
                            cb(null,{
                                email: mail,
                                valid: true
                            })
                        } else {
                            serverwiseStats({
                                invalid: 1,
                                total: 1
                            }, proc, server);
                            mail_wiseStats({
                                invalid: 1,
                                total: 1
                            }, account_mail, proc);
                            id_daywiseStats({
                                invalid: 1,
                                total: 1
                            }, proc);
                            cb(null,{
                                email: mail,
                                valid: false
                            })
                        }
                    } else {
                        console.log("error--------invalid cookies  or token", body, response.statuCode);
                        /*  unvalidated_mails.insertOne({
                         _id: res._id
                         }, function(err, ok) {
                         cb({
                         email: mail,
                         valid: false
                         })
                         })*/
                    }
                }

                request(options, callback);
            }

            var m_count = 0;

            function rotate() {
                tc_mail.find({}).limit(100).toArray(function (err, docs) {
                    if (err)
                        console.log(err);
                    else {
                        if (docs.length > 0) {
                            async.each(docs, function (res, cb) {
                                    // console.log('*******************************', res);
                                    var gmail = res._id.toLowerCase().trim();
                                    g_validation(gmail, function (err,data) {
                                        if (!err && data) {
                                            data1.update({
                                                _id: res._id
                                            }, {
                                                $set: {
                                                    valid: data.valid,
                                                    g4: getMHD(new Date()).d,
                                                    updated_on: getMHD(new Date()).d,
                                                    validated_by: proc
                                                },
                                                $unset: {
                                                    lock: ''
                                                }

                                            }, function (err, ree) {
                                                if (err)
                                                    console.log(err);
                                                else {
                                                    var incmt_doc = {
                                                        total: 1
                                                    };
                                                    if (data.valid) {
                                                        incmt_doc['valid'] = 1;
                                                    } else {
                                                        incmt_doc['invalid'] = 1;
                                                    }
                                                    processes.updateOne({
                                                        process: proc,
                                                        server: server
                                                    }, {
                                                        $inc: incmt_doc
                                                    })
                                                    tc_mail.remove({
                                                        _id: res._id
                                                    }, function (err, jkl) {
                                                        if (err)
                                                            console.log(err);
                                                        else
                                                            cb();
                                                    });
                                                }
                                            });
                                            console.log(data);
                                        } else {
                                            if(err=='token expired'){
                                                accounts.updateOne({token:doc.token},{$set:{active:false}})
                                            }
                                            cb("error")
                                        }
                                    })

                                    // request(options, callback);

                                },
                                function (err) {
                                    if (err) {
                                        console.log(err);
                                        setTimeout(token_rotation, 0);
                                    } else {
                                        console.log('loop completed');
                                        setTimeout(rotate, 0);
                                    }
                                })

                        } else {
                            console.log('db completed and retrying');
                            setTimeout(rotate, 0);
                        }
                    }
                });
            }

            test();
            //rotate();


            } else {
                accounts.updateMany({
                    server: server,
                    process: proc,
                    active: true,
                    use: 1
                }, {
                    $set: {use: null}
                }, function (err, ok) {
                    if (!err && ok && ok.result.nModified > 0) {
                        logger.log("rotation completed");
                        setTimeout(token_rotation, 0);
                    } else if (!err && ok && ok.result.nModified == 0) {
                        logger.log("no tokens");
                        setTimeout(token_rotation, 100);
                    } else {
                        logger.log(err);
                        setTimeout(token_rotation, 100);
                    }
                })

            }
        }else {
            logger.log(err);
        }
        });
    }
            setTimeout(token_rotation,0);

        }
    })
});
