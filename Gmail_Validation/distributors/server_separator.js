var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
var async = require('async');
var config = require('./config.json');

function start_DB(host, db, username, password, cb) {
    MongoClient.connect("mongodb://" + username + ":" + password + "@" + host + "/" + db, function(err, db_conn) {
        if (err) {
            cb(err);
        } else {
            cb(null, db_conn);
        }
    });
}

process.on('uncaughtException', function(err) {
    if (err) {
        console.log(err);
        process.exit(0);
    }
});

start_DB(config.main_db.ip + ":" + config.main_db.port, config.main_db.db, config.main_db.username, config.main_db.password, function(err, gsb) {
    if (err) {
        console.log(err);
        process.exit(0);
    }
    var processes = gsb.collection(config.main_db.processes_collection);
    var data = gsb.collection(config.main_db.data_collection);

    function transport() {
        console.log('transporter started');
        processes.find({
            priority: {
                $ne: null
            },
            active: true
        }).sort({
            priority: -1
        }).toArray(function(err, docs) {
            if (docs) {
                console.log('records found in processes', docs);
                // process.exit(0);
                async.each(docs, function(doc, cbk) {
                    if (doc) {
                        var served = 0;
                        console.log(doc);
                        var limit = doc.buffer;
                        var host = doc.info.host;
                        var host_db = doc.info.db;
                        var username = doc.info.username;
                        var password = doc.info.password;
                        var host_count;
                        start_DB(host, host_db, username, password, function(err, hostdb) {
                            if (err) {
                                console.log(err);
                                process.exit(0);
                            }
                            if (hostdb) {
                                var process_collection = hostdb.collection(doc.info.collection);
                                var cursor;

                                function find_query() {
                                    cursor = process_collection.find({
                                        $or: [{
                                            valid: true
                                        }, {
                                            valid: false,
                                            g3: {
                                                $ne: null
                                            },
                                            g4: {
                                                $ne: null
                                            },
                                            g2: {
                                                $ne: null
                                            },
                                            g5: {
                                                $ne: null
                                            }


                                        }]
                                    }).limit(100);
                                    cursor.nextObject(function(err, item) {
                                        if (item) {
                                            cursor.rewind();
                                            setTimeout(next_item, 10);
                                        } else {
                                            console.log("records not found requerying");
                                            hostdb.close();
                                            cbk();
                                        }
                                    });
                                }

                                setTimeout(find_query, 0);

                                function next_item() {
                                    cursor.nextObject(function(err, item) {
                                        if (item) {
                                            var itemKey = "validEmails." + item.type + "." + item.index;
                                            if (item.valid == true) {
                                                data.update({
                                                        _id: item.lid
                                                    }, {
                                                        $set: {
                                                            [itemKey]: item._id
                                                        }
                                                    },
                                                    function(err, result) {
                                                        console.log(result.ok);
                                                        if (!err) {
                                                            process_collection.remove({
                                                                _id: item._id
                                                            }, function(err, ok) {
                                                                if (!err) {
                                                                    console.log('query------------');
                                                                    console.log('valid true correctly inserted');
                                                                }
                                                            });
                                                        } else {
                                                            console.log("ERROR while inserting");
                                                        }
                                                        setTimeout(next_item, 0);
                                                    });
                                            } else {
                                                process_collection.remove({
                                                    _id: item._id
                                                }, function(err, ok) {
                                                    if (!err) {
                                                        console.log('query------------');
                                                        console.log('valid false correctly inserted');
                                                    }
                                                });
                                                setTimeout(next_item, 0);
                                            }
                                        } else {
                                            console.log('nextobject not found');
                                            hostdb.close();
                                            cbk();
                                        }
                                    });
                                }

                            } else {
                                console.log('db error', err);
                            }
                        });
                    }
                }, function(err) {
                    if (!err) {
                        console.log('completed');
                        setTimeout(transport, 0);
                    } else {

                    }
                });
            } else {
                console.log('no records found');
            }
        });
        // setTimeout(transport,10000);
    }

    transport();
});
