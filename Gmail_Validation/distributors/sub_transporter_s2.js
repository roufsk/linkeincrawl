var MongoClient = require('mongodb').MongoClient,
	assert = require('assert');
var async = require('async');
var config = require('./config.json');
var logger = require('tracer').console({
	format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
	dateformat: "dd-mm-yyyy HH:MM:ss TT"
});
// logger.log(process.argv);
function start_DB(host, db, username, password, cb) {
	MongoClient.connect("mongodb://" + username + ":" + password + "@" + host + "/" + db, function(err, db_conn) {
		if (err) {
			cb(err);
		} else {
			cb(null, db_conn);
		}
	});
}

process.on('uncaughtException', function(err) {
	if (err) {
		logger.log(err);
		process.exit(0);
	}
});
start_DB(config.main_db.ip + ":" + config.main_db.port, config.main_db.db, config.main_db.username, config.main_db.password, function(err, gsb) {
	if (err) {
		logger.log(err);
		process.exit(0);
	}
	var processes = gsb.collection(config.main_db.all_processes_collection);
	var data = gsb.collection("s2");


	function transport() {
		logger.log('transporter started');
		processes.find({
			server: "s2",
			priority: {
				$ne: null
			},
			active: true
		}).sort({
			priority: -1
		}).toArray(function(err, docs) {
			if (docs) {
				logger.log('records found in processes', docs);
				// process.exit(0);
				async.eachSeries(docs, function(doc, cbk) {
					if (doc) {
						var served = 0;
						logger.log(doc);
						var limit = doc.buffer;
						var host = doc.info.host;
						var host_db = doc.info.db;
						var username = doc.info.username;
						var password = doc.info.password;
						var host_count;
						start_DB(host, host_db, username, password, function(err, hostdb) {
							if (err) {
								logger.log(err);
								process.exit(0);
							}
							if (hostdb) {
								var process_collection = hostdb.collection(doc.info.collection);
								// process_collection.insert({_id:'dnhejdfc@jvc.com'},);
								process_collection.count(function(err, count) {
									host_count = count;
									logger.log('host count:' + host_count + ' limit:' + limit);
									if (host_count < limit) {
										var remaining = limit - host_count;
										logger.log('remaining:', remaining);
										// process.exit(0);
										logger.log(doc.query);
										var duplicate;
										var fields = doc.fields;

										function copy_paste(cb) {
											if (remaining > 0) {
												duplicate = 0;
												async.eachSeries(doc.query, function(q, callback) {
													// callback;
													logger.log("coming here", q);
													q = JSON.parse(q);
													var fq = {};
													// fq['p'] = null;
													fq['lock'] = {
														$ne: true
													};
													Object.keys(q).forEach(function(key, value) {
														fq[key] = q[key];
													});
													if (remaining > 0) {
														logger.log(fq);
														logger.log(fields);
														var query_limit = remaining < 500 ? remaining : 500;
														data.find(fq, fields).limit(query_limit).toArray(function(err, docs) {
															if (docs) {
																async.each(docs, function(item, ncb) {
																	if (remaining > 0) {
																		process_collection.insert(item, function(err, result) {
																			if (!err) {
																				remaining--;
																				data.update({
																					_id: item._id
																				}, {
																					$set: {
																						lock: true
																					}
																				}, function(err, ok) {
																					if (!err) {
																						served++;
																						logger.log('served', served);
																						processes.update({
																							_id: doc._id
																						}, {
																							$inc: {
																								'served': 1
																							}
																						}, function(err, ok) {
																							if (!err) {
																								ncb();
																							} else {
																								logger.log("updation error");
																							}
																						});
																						logger.log('duplicates:', duplicate);
																						logger.log('remaining:', remaining);
																						logger.log('query------------', fq);
																						logger.log('correctly inserted');
																					} else {
																						logger.log("insertion error");
																					}
																				});
																			} else {
																				duplicate++;
																				logger.log('insert errr:', err);
																				data.update({
																					_id: item._id
																				}, {
																					$set: {
																						lock: true
																					}
																				}, function(err, ok) {
																					if (!err) {
																						logger.log('duplicates:', duplicate);
																						logger.log('remaining:', remaining);
																						logger.log('query------------', fq);
																						logger.log('incorrectly inserted');
																						ncb();
																					} else {
																						logger.log("updation error");
																					}
																				})
																			}

																		});
																		logger.log(item)
																	} else {
																		// callback();
																		ncb();
																	}
																}, function(err) {
																	if (!err) {
																		callback();
																	}
																});
															}
														});
													} else {
														callback();
													}

												}, function(err) {
													logger.log('got it');
													if (!err) {
														if (duplicate == 0) {
															logger.log('no duplicates');
															return cb();
														} else {
															logger.log('yes duplicates');
															setTimeout(
																function() {
																	copy_paste(function(err, result) {
																		if (!err) {
																			cb();
																		}
																	});
																}, 1000);
														}

													} else {
														logger.log(err, "<---------");
													}

												});
											} else {
												cb();
											}

										}

										if (remaining > 0) {
											copy_paste(function(err, result) {
												if (!err) {
													logger.log('one array element completed');
													if (remaining > 0) {
														logger.log('remaining gt 0');
														// processes.update({_id: 'main'}, {$inc: {'balance.any': remaining}});
														hostdb.close();
														cbk();
													} else {
														logger.log('remaining not gt 0');
														hostdb.close();
														cbk();
													}
												}
											});
										} else {
											hostdb.close();
											cbk();
										}
									} else {
										hostdb.close();
										cbk();
									}
								});

							} else {
								logger.log('db error', err);
							}
						});
					}
				}, function(err) {
					if (!err) {
						logger.log('completed');
						setTimeout(transport, 0);
					} else {

					}
				});
			} else {
				logger.log('no records found');
			}
		});
		// setTimeout(transport,10000);
	}

	transport();
});
