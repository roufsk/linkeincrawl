var MongoClient = require('mongodb').MongoClient;
var async = require('async');
var gsburl = "mongodb://user:1ZoT4a.6iZ@209.58.136.13:27017/rocketreach";

function getNextSequenceValue(name, cb) {
	countersCollection.findAndModify({
			_id: name
		}, [
			['_id', 'asc']
		], {
			$inc: {
				seq: 1
			}
		}, {
			new: true,
			upsert: true
		},
		function(err, ret) {
			if (!err) cb(err, ret.value.seq);
			else cb(err, null);
		});


}

function iterate() {
	function addSeq(err, seq) {
		console.log(seq);
		predictions2.findAndModify({
			seqNum: null
		}, {}, {
			$set: {
				"seqNum": seq
			}
		}, function(err, ok) {
			// if (!err)
			// 	iterate();
		})
		iterate();
	}
	getNextSequenceValue("seqNum", function(err, seq) {
		addSeq(err, seq);
	});

}

function createCollection(cb) {
	MongoClient.connect(gsburl, function(err, db) {
		if (err) throw err;
		predictions2 = db.collection("predictions2");
		countersCollection = db.collection("counters");
		cb();
	});
};
createCollection(function() {
	iterate();
});
