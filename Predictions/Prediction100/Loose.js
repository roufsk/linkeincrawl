var MongoClient = require('mongodb').MongoClient;
//DB Connection
var async = require('async');
var url = "mongodb://user:1ZoT4a.6iZ@209.58.136.13:27017/rocketreach";
var sleep = require('sleep');
var trashwords = require("./trashwords.js")
var multiword = require("./DealingWithMultipleWords.js")
var getnickname = require("./nickname.js")
var emails = []
var extraGmails = []


/*TimeStamp*/
function getTodayDateLong() {
  var date = new Date().toISOString().substr(0, 10);
  return Math.floor((new Date(date + " 00:00:00")).getTime() / 1000);
}

/*MongoConnection*/

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  console.log("Database connected!");
  var i = 0;
  var pred;
  var domain;
  /*Calling Predictions Process*/
  process();

  function process() {
    /*Records having lock true means already verified*/
    console.log("1000 over");
    sleep.sleep(2);
    db.collection("predictions2").find({
      "lock": true
    }, {
      "_id": 1
    }).limit(1000).toArray(function(err, result) {
      if (err) {
        console.log("Error occured in getting lock true");
        throw err;
      } else {
        async.each(result, function(item, cb) {
          console.log(item);
          db.collection("employee").findOne({
            "lid": item["_id"]
          }, function(err, item2) {
            // console.log(item2);
            if (item2 !== null)
              after(item2.name, item2.lid);
            cb();
          });
        }, function(err) {
          if (!err)
            process();
        });
      }
    });
  }
  /*After SubProcess of removing trashwords and multinames*/
  function after(EmployeeName, EmployeeLid) {
    EmployeeName = trashwords(EmployeeName)
    var nickname = getnickname(EmployeeName)
    EmployeeName = multiword(EmployeeName)
    var i;
    for (i = 0; i < EmployeeName.length; i++) {
      redundant(EmployeeName[i]);
    }
    thatnext(domain, emails, EmployeeLid, nickname);
    emails = [];
  }
  /*That Next*/
  function thatnext(domain, emails, EmployeeLid, nickname) {
    var leng = emails.length;
    var j;
    var dic1 = {};
    var dic2 = {};
    /* for (j = 0; j < leng; j++) {
         dic1[j] = emails[j] + "@" + domain;
         dic2[j] = emails[j] + "@" + "gmail.com";
     }*/
    j = emails.length;
    nickname=nickname.toLowerCase();
    emails.push(nickname);
    dic2[j] = nickname + "@gmail.com"
    var k = j + 1
    for (j = 0; j < leng + 1; j++) {

      for (var i = 0; i <= 10; i++) dic2[k++] = emails[j] + i.toString() + "@gmail.com"
    }
    db.collection('predictions2').update({
      "_id": EmployeeLid
    }, {
      $set: {
        "ctdOn": getTodayDateLong(),
        "lock": false,
        "gmailPredictions": dic2
      }
    }, function(err) {
      if (err)
        throw err;
      else {
        console.log("Inserted");
      }
    })
  }


  /*Guessing*/
  function redundant(EmployeeName) {
    if (EmployeeName)
      console.log(EmployeeName);
    EmployeeName = EmployeeName.toLowerCase();
    EmployeeName = EmployeeName.replace(/^\s+|\s+$/g, '').split(" ");
    if (EmployeeName.length == 1) {
      guessing(EmployeeName[0], "")
    } else {
      guessing(EmployeeName[0].replace(/^\s+|\s+$/g, ''), EmployeeName[1].replace(/^\s+|\s+$/g, ''));
      guessing(EmployeeName[1].replace(/^\s+|\s+$/g, ''), EmployeeName[0].replace(/^\s+|\s+$/g, ''));
    }
    return;
  }


  /*Pattern Producing*/
  function guessing(f, l) {
    if (f.length > 0 && l.length > 0) {
      emails.push(f + l);
      emails.push(f + '.' + l);
      emails.push(f + l.substring(0, 1));
      emails.push(f + '.' + l.substring(0, 1));
      emails.push(f.substring(0, 1) + l);
      emails.push(f.substring(0, 1) + '.' + l);
      emails.push(f);
    } else if (f.length > 0) {
      emails.push(f);
    } else if (l.length > 0) {
      emails.push(l);
    }
    return;
  }
});
