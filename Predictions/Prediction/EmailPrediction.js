var MongoClient = require('mongodb').MongoClient;
//DB Connection
var url = "mongodb://user:1ZoT4a.6iZ@209.58.136.13:27017/rocketreach";
//Process Starts
var emails = []
var sleep = require('sleep');
var trashwords = require("./trashwords.js")
var multiword = require("./DealingWithMultipleWords.js")

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    console.log("Database connected!");
    //processing each time some limit employees
    var i = 0;
    var pred;
    var domain;
    db.collection("mxStats").findOne({
        "_id": 1
    }, {
        "predcount": 1
    }, function(err, item) {
        pred = item.predcount;
        //after();


    });

    function guessing(f, l) {
        if (f.length > 0 && l.length > 0) {
            emails.push(f + l);
            emails.push(f + '.' + l);
            //emails.push(f+'-'+l);
            emails.push(f + '_' + l);
            emails.push(f + l.substring(0, 1));
            emails.push(f + '.' + l.substring(0, 1));
            //emails.push(f+'-'+l.substring(0,1));
            emails.push(f + '_' + l.substring(0, 1));
            emails.push(f.substring(0, 1) + l);
            emails.push(f.substring(0, 1) + '.' + l);
            //emails.push(f.substring(0,1)+'-'+l);
            emails.push(f.substring(0, 1) + '_' + l);
            emails.push(f);
        } else if (f.length > 0) {
            emails.push(f);
        } else if (l.length > 0) {
            emails.push(l);
        }
        return;
    }

    function thatnext(domain, emails, EmployeeLid) {
        var leng = emails.length;
        var j;
        var dic1 = {};
        var dic2 = {};
        for (j = 0; j < leng; j++) {
            dic1[j] = emails[j] + "@" + domain;
            dic2[j] = emails[j] + "@" + "gmail.com";
            //dic1.push (j,emails[j]+"@"+domain);
            //dic2.push(j,emails[j]+"@"+"gmail.com");
            //console.log(j+"  "+emails[j]+"@"+domain);
            //console.log("\n");
        }
        //console.log(dic1);
        //console.log(dic2);
        db.collection("predictions2").insert({
            "_id": EmployeeLid,
            "gmailPredictions": dic2,
            "companyPredictions": dic1,
            "status": -1
        });
        return;
    }


    function redundant(EmployeeName) {
        console.log(EmployeeName);
        EmployeeName = EmployeeName.toLowerCase();
        EmployeeName = EmployeeName.replace(/^\s+|\s+$/g, '').split(" ");
        if (EmployeeName.length == 1) {
            guessing(EmployeeName[0], "")
        } else {
            guessing(EmployeeName[0].replace(/^\s+|\s+$/g, ''), EmployeeName[1].replace(/^\s+|\s+$/g, ''));
            guessing(EmployeeName[1].replace(/^\s+|\s+$/g, ''), EmployeeName[0].replace(/^\s+|\s+$/g, ''));
        }
        // console.log(EmployeeName);
        return;
    }

    function after(EmployeeName, EmployeeCid, EmployeeLid) {
        db.collection("company").findOne({
            "cid": EmployeeCid
        }, {
            "domain": 1
        }, function(err, res) {
            domain = res.domain;
            domain = domain.split("//");
            if (domain[1].substring('www') !== -1) {
                domain = domain[1].substring(4);
                domain = domain.split("/")[0];
            } else {
                domain = domain[1].split("/")[0];
            }
            //console.log(EmployeeName+"   "+EmployeeCid+"  "+EmployeeLid);
            EmployeeName = trashwords(EmployeeName)
            EmployeeName = multiword(EmployeeName)
                //console.log(EmployeeName);
            var i;
            for (i = 0; i < EmployeeName.length; i++) {
                //console.log(EmployeeName[i]);
                redundant(EmployeeName[i]);
            }
            //redundant(EmployeeName);

            //console.log(emails+" "+domain);
            thatnext(domain, emails, EmployeeLid);
            emails = []
        });
        return;
    }

    function process() {
        db.collection("employee").find({
            "$and": [{
                "loc.cy": "India"
            }, {
                "df": null
            }]
        }, {
            "name": 1,
            "lid": 1,
            "cid": 1
        }).limit(10000).toArray(function(err, result) {
            if (err) throw err;
            if (result.length < 1000) {
                console.log("I don't have enough work i am sleeping");
                sleep.sleep(3600);
            }
            var j;
            var len = result.length;
            //console.log(len);
            for (j = 0; j < len; j++) {
                //console.log(result[j]);
                var EmployeeName = result[j].name;
                var EmployeeCid = result[j].cid;
                var EmployeeLid = result[j].lid;
                db.collection("employee").updateOne({
                    "lid": EmployeeLid
                }, {
                    "$set": {
                        "df": 1
                    }
                });
                after(EmployeeName, EmployeeCid, EmployeeLid);
                process();
            }

        });
        i = i + 1;
    }
    // cb();
});