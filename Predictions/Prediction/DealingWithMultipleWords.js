function multiWords(EmployeeName)
{
	var name_list=[];
	EmployeeName=EmployeeName.replace(/^\s+|\s+$/g, '');
	var TempList=EmployeeName.split(" ");
	if(TempList.length==1)
	{
		return TempList;
	}
	//console.log(TempList);
	var i;
	for(i=0;i<3;i++)
	{
		//console.log(TempList[i])
		if(TempList[i])
		{
			name_list.push(TempList[i]);
		}
	}
	var len=name_list.length;
	var NewList=[];
	var first="";
	var last="";
	for(i=0;i<len-1;i++)
	{
		first=name_list.slice(0,i+1).join('')
		last=name_list.slice(i+1,len).join('')
		NewList.push(first+" "+last)
	}
	return NewList;
}
module.exports=multiWords;
