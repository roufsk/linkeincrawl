var request = require('request');
var cheerio = require('cheerio');
var Encoder = require('html-entities');
var Entities = require('html-entities').AllHtmlEntities;
var entities = new Entities();
var MongoClient = require('mongodb').MongoClient;
var config = require('./config.json');
var cookieUrl = "mongodb://" + config.cookieDB.username + ":" + config.cookieDB.password + "@" + config.cookieDB.ip + ":" + config.cookieDB.port + "/" + config.cookieDB.which_db + "?authSource=admin";
var url = "mongodb://" + config.server.username + ":" + config.server.password + "@" + config.server.ip + ":" + config.server.port + "/" + config.server.which_db + "?authSource=admin";
var locationObj = {},
	peopleObj = {},
	peopleCount = 0;
var cid, thisCookie, reqCount = 0,
	arr, uid, increment = 0,
	pageOver,
	pageIndex, cookieVal = 0,
	companyCount = Number(process.argv[2]),
	cookieLimit = Number(process.argv[3]);
var cmpCollection, empCollection, countersCollection;
var header = {
	// 'accept-encoding': 'gzip, deflate, sdch, br',
	'accept-language': 'en-GB,en-US;q=0.8,en;q=0.6',
	'upgrade-insecure-requests': '1',
	'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36',
	'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
	'cache-control': 'max-age=0',
	'authority': 'www.linkedin.com',
	'cookie': '',
	'referer': 'https://www.linkedin.com/'
};

function safelyParseJSON(json) {
	var parsed
	try {
		parsed = JSON.parse(json)
	} catch (e) {}
	return parsed
}

function getTodayDateLong() {
	var date = new Date().toISOString().substr(0, 10);
	return Math.floor((new Date(date + " 00:00:00")).getTime() / 1000);
};

function createCollection(callback) {
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		db1 = db;
		empCollection = db.collection("employee");
		countersCollection = db.collection("counters");
		cmpCollection = db.collection("company");
		callback();
	});
}

function getCookieVal(callbackfunction) {
	MongoClient.connect(cookieUrl, function(err, db) {
		if (err) throw err;
		cookieCollection = db.collection("cookies");
		cookieCollection.findAndModify({
			timestamp: null,
			cookies: {
				$ne: {}
			},
			connections: {
				$gt: 100
			},
		}, {}, {
			$set: {
				timestamp: parseInt(new Date() / 1000)
			}
		}, function(err, item) {
			console.log('here', item);
			thisCookie = item["value"]["cookies"];
			header["cookie"] = 'JSESSIONID="' + thisCookie.JSESSIONID + '";bcookie="' + thisCookie.bcookie + '";bscookie="' + thisCookie.bscookie + '";lang="' + thisCookie.lang + '";li_at="' + thisCookie.li_at + '";liap="' + thisCookie.liap + '";lidc="' + thisCookie.lidc + '";_lipt="' + thisCookie._lipt + '";visit="' + thisCookie.visit + '";sdsc="' + thisCookie.sdsc + '";sl="' + thisCookie.sl + '";_ga="' + thisCookie._ga + '"';
			callbackfunction();
		});

	})
}

function getCidVal(callback) {
	getNextSequenceValue('cmpltd', function(err, id) {
		cmpCollection.find({
			"_id": id
		}).toArray(function(err, item) {
			console.log(item);
			cid = item[0]["cid"];
			callback();
		})
	});
}

function cidIterator() {
	pageOver = false;
	pageIndex = 1;
	if (increment < (companyCount)) {
		console.log(increment);
		getCidVal(function() {
			requestIterator(pageIndex);
		});

	} else {
		cookieCollection.findAndModify({
			cookies: thisCookie
		}, {}, {
			$set: {
				"req": reqCount
			}
		});
		cookieIterator();
	}
	increment++;
}

function cookieIterator() {
	console.log(cookieVal);
	if (cookieVal < cookieLimit) {
		getCookieVal(function() {
			increment = 0;
			reqCount = 0;
			cidIterator();
		});
		cookieVal++;
	} else {
		return;
	}
}
createCollection(function() {
	cookieIterator();
})

function insertData(myobj) {
	getNextSequenceValue('empDetails', function(err, id) {
		myobj['_id'] = id;
		empCollection.insertOne(myobj);
		console.log(id + ' rec Inserted')
	});
}

function getNextSequenceValue(name, cb) {
	countersCollection.findAndModify({
			_id: name
		}, [
			['_id', 'asc']
		], {
			$inc: {
				seq: 1
			}
		}, {
			new: true,
			upsert: true
		},
		function(err, ret) {
			if (!err) cb(err, ret.value.seq);
			else cb(err, null);
		});


}



function locationFinder(index) {
	if (arr[index]) {
		if (arr[index]["location"]) {
			locationObj[arr[index]["miniProfile"]] = arr[index]["location"];
		}
		if (Object.keys(locationObj).length < 10) {
			locationFinder(index + 1);
		} else {
			return;
		}
	} else {
		return;
	}
}

function peopleObjCreator(i) {
	if (arr[i]) {
		if (arr[i]["entityUrn"] && locationObj[arr[i]["entityUrn"]]) {
			peopleObj = arr[i];
			peopleObj["location"] = locationObj[arr[i]["entityUrn"]];
			var locArr = peopleObj["location"].split(', ');
			var prfsn;
			locArr[0] = locArr[0].split(' Area')[0];
			if (peopleObj["occupation"].includes(" at ")) {
				prfsn = peopleObj["occupation"].split(' at ')[0];
			} else if (peopleObj["occupation"].includes(" in ")) {
				prfsn = peopleObj["occupation"].split(' in ')[0];
			} else if (peopleObj["occupation"].includes(" @ ")) {
				prfsn = peopleObj["occupation"].split(' @ ')[0];
			} else {
				prfsn = peopleObj["occupation"] || "";
			}
			var resObj = {
				'lid': peopleObj["publicIdentifier"],
				'fName': peopleObj["firstName"] || "",
				'lName': peopleObj["lastName"] || "",
				'cid': cid,
				'prfsn': prfsn || "",
				'loc': {
					'ct': locArr[0] || "",
					'cy': locArr[1] || ""
				},
				'ctdOn': getTodayDateLong(),
				'status': 0
			};
			if (resObj["lid"] !== "UNKNOWN")
				insertData(resObj);
			peopleCount++;
		}
		if (peopleCount < 10) {
			peopleObjCreator(i + 1);
		} else {
			return;
		}
	} else {

	}

}

function clearCidVal(cb) {
	empCollection.findAndModify({
		"cid": cid
	}, {}, {
		$unset: {
			"cmpltd": ""
		}
	}, function() {
		cb();
	});
}

function callback(error, response, html) {
	locationObj = {};
	console.log('pageIndex', pageIndex);
	if (!error && response.statusCode == 200) {
		var res = entities.decode(html);
		var $ = cheerio.load(html);
		var b = $('code');
		for (var j = 0; j < b.length; j++) {
			var newres = $(b[j]).html();
			var nextres = entities.decode(newres);
			if (typeof(nextres) == "string") {
				if (safelyParseJSON(nextres) !== undefined) {
					var thisObj = JSON.parse(nextres);
					if (thisObj["data"]) {
						if (thisObj["data"]["metadata"]) {
							var dataObj = thisObj;

						}
					}

				}
			}
		}
		if (dataObj) {
			arr = dataObj['included'];
			if (arr.length == 0) {
				pageOver = true;
			}
			locationFinder(arr.length - 30);
			peopleObjCreator(0);
		}

		setTimeout(function() {
			requestIterator(++pageIndex);
		}, 0);
	} else {
		cookieVal--;
		clearCidVal(function() {
			cookieIterator();
		});
	}
}

function requestIterator(pageIndex) {
	if (pageIndex <= 100 && pageOver == false) {
		peopleCount = 0;
		reqCount++;
		if (pageIndex == 1) {
			var url = 'https://www.linkedin.com/search/results/people/?facetCurrentCompany=%5B%22' + cid + '%22%5D';
		} else {
			var url = 'https://www.linkedin.com/search/results/people/?facetCurrentCompany=%5B%22' + cid + '%22%5D&page=' + pageIndex;
		}
		var options = {
			url: url,
			headers: header,
		};
		request(options, callback);

	} else {
		cidIterator();
	}


}