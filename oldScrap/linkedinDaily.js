var request = require('request');
var cheerio = require('cheerio');
var increment = 0;
var config = require('./likeincrawl/config.json');
// var utils = require('./utils.js');
var cookieUrl = "mongodb://" + config.cookieDB.username + ":" + config.cookieDB.password + "@" + config.cookieDB.ip + ":" + config.cookieDB.port + "/" + config.cookieDB.which_db + "?authSource=admin";
var url = "mongodb://" + config.server.username + ":" + config.server.password + "@" + config.server.ip + ":" + config.server.port + "/" + config.server.which_db + "?authSource=admin";
var MongoClient = require('mongodb').MongoClient;
var Encoder = require('html-entities');
var Entities = require('html-entities').AllHtmlEntities;
var entities = new Entities();
var fixedReqCount = Number(process.argv[2]);
var cookieLimit = Number(process.argv[3]);
var arrIndex = [0, 1];
var collection, db1, count, thisCompId, compCount, resObj, countersCollection, reqCount = 0,
	thisCookie;
var locArr = ["ct", "cy", "pCode"];
var keys = ["_id", "cid", "name", "type", "empSize", "empSizeInfo", "domain", "loc", "followers"];
var vals = ["", "url", 'name', "industries", "staffCount", "staffCount",
	"companyPageUrl", ["geographicArea", "country", "postalCode"], "followerCount"
];
var cookieVal = 1;
var resCookie;
var header = config.header;

function getNextSequenceValue(name, cb) {
	countersCollection.findAndModify({
			_id: name
		}, [
			['_id', 'asc']
		], {
			$inc: {
				seq: 1
			}
		}, {
			new: true,
			upsert: true
		},
		function(err, ret) {
			if (!err) cb(err, ret.value.seq);
			else cb(err, null);
		});


}

function safelyParseJSON(json) {
	var parsed
	try {
		parsed = JSON.parse(json)
	} catch (e) {}
	return parsed
}

function getTodayDateLong() {
	var date = new Date().toISOString().substr(0, 10);
	return Math.floor((new Date(date + " 00:00:00")).getTime() / 1000);
};

function createCookieObj(cbfun) {
	MongoClient.connect(cookieUrl, function(err, db) {
		if (err) throw err;
		// cookieCollection = db.collection("cookies");
		// cookieCollection.findAndModify({
		// 	timestamp: null
		// }, {}, {
		// 	$set: {
		// 		timestamp: parseInt(new Date() / 1000)
		// 	}
		// }, function(err, item) {
			cookieObj = {"_cap_profile":"t=MINI","bcookie":"v=2&8155195d-9445-478f-827b-f33a5754b01b","bscookie":"v=1&20170713140956f438c40b-8e85-420d-8df4-345232f87c94AQHrEuLZvka8nAJbwsIQ6jZn0KvCIU0B","visit":"v=1&M","VID":"V_2017_07_13_21_356","_cb_ls":"1","_chartbeat2":"Bm_xz8DrVMGjDtog0v.1500363256753.1500363378038.1","hc_survey":"true","_gat":"1","sl":"v=1&hKbSb","li_at":"AQEDASNP7mQBqkx5AAABXYktnSEAAAFdiuURIU0AFMa0Od02EDX8o8Q_7lD6qRydGgQuZcEzH6Lu_VigwIBbVq6a8LiY7He3AOkEaBq86O7KDoP_zxIlqbGtYBH5EvU5ByZSVRtKhdOHW31Y4zLWkJd9","liap":"true","JSESSIONID":"ajax:2483602934481973873","lidc":"b=VGST07:g=408:u=1:i=1501245054:t=1501331454:s=AQFWuPlCQbMPS3PLT-3SuaeaKa30PjCw","transaction_state":"AgE9fKhdumF_sgAAAV2JMQbvYDgWGSPvS2z3ONYF12zpK_z3-HXeuMhZUP44ZJgOTF-zna0WipyT741M53Jlzpqsiw7w9RChdORtsAxd2O_KgqV1otwVPKG9sob4rq5EvJvgTSwgouCgYNlThU5PHr_a4YpFp4whjCsJ7uyjMgy9pCJci5vzy0gILCEUqn62-SzpR4fBcllaD8pAyrXw1_ZxN9JkIS986HQqVMF9AbhOdcI_b56KmkB-L5u8BCqeHfE4jIf1_1hOc8LnyYyVOE6P4T9cxFY_MPYv1UIncntODq2g_MFFSs3RHurl6v4_nS2vHsJOosMNtpFRi7aVLCG6lYI7XCulnISggSCL6ByqcpshxeBeA1iq6D3BFwN50Cu2t2xxzl42Jr1vlmde1zHIeQ","lang":"v=2&lang=en-us&fid=a9dce662df5946a8bafa99e436b4c22b","RT":"s=1501245301889&r=https%3A%2F%2Fwww.linkedin.com%2Fstart%2F%3FlastWidget%3Duno_goals_takeover","_ga":"GA1.2.666875041.1499955054","_lipt":"CwEAAAFdiTF6g6wHQI6fQQDYdSiw-YKWqcnAX0QO0GKOY4Kt-erU0B7HB5Scbcq1UrlSvg7wBrapwmIf1zGO6xacxWdJjZdRd2zfmHiKnnMoWkDRNW0kBKEroVw"}
;
		// 	reqCount = 0;
		// 	console.time('cbfun');
			cbfun();
		// 	console.timeEnd('cbfun');
		// });

	})
};

function createCollection(cb) {
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		db1 = db;
		increment = 0;
		collection = db.collection("company");
		countersCollection = db.collection("counters");
		cb();

	});
}

function cookieIterate() {
	thisCookie = cookieObj;
	// var thisCookie = {"_cap_profile":"t=MINI","bcookie":"v=2&8155195d-9445-478f-827b-f33a5754b01b","bscookie":"v=1&20170713140956f438c40b-8e85-420d-8df4-345232f87c94AQHrEuLZvka8nAJbwsIQ6jZn0KvCIU0B","visit":"v=1&M","VID":"V_2017_07_13_21_356","_cb_ls":"1","_chartbeat2":"Bm_xz8DrVMGjDtog0v.1500363256753.1500363378038.1","u_tz":"GMT+0530","lang":"v=2&lang=en-us","li_at":"AQEDASDf9nACRk4ZAAABXXMDXe4AAAFddLrR7lEAG6T9xjgt9rUX28erhuK_K5LE8D2EUuq8w7Kwhd19pJsOYZ-JefUS8ksvvDnQgLF6zqewcskY2VYOILlrSi1X5vGzCz1qsM3e1kNrH9GErq6rh3Iz","JSESSIONID":"ajax:3434293117762422017","liap":"true","sl":"v=1&qaKzZ","_ga":"GA1.2.666875041.1499955054","_lipt":"CwEAAAFddG91DceVcMk1wJ5SSmOcU6l1O-hNBpCTxAHrU-oNvwyzlcsOMfNJCzMxwG7BUCcDf8nCE78EQWPDCSgcjurwPunXP_BWMyrbbD9TdgG9TsV_Xw20zmx_pNmk3LNHBsLDSJDZH1m9fmWhPbtBQbuRD2XAVnVCmABBKG3m-wbWsiLKPpO_sj0jzX_8SZEDDM19r0SF1A"};
	if (thisCookie !== undefined && Object.keys(thisCookie).length !== 0) {
		console.log(cookieObj["username"]);
		resCookie = 'JSESSIONID="' + thisCookie.JSESSIONID + '";bcookie="' + thisCookie.bcookie + '";bscookie="' + thisCookie.bscookie + '";lang="' + thisCookie.lang + '";li_at="' + thisCookie.li_at + '";liap="' + thisCookie.liap + '";lidc="' + thisCookie.lidc + '";_lipt="' + thisCookie._lipt + '";visit="' + thisCookie.visit + '";sdsc="' + thisCookie.sdsc + '";sl="' + thisCookie.sl + '";_ga="' + thisCookie._ga + '"';
		header['cookie'] = resCookie;
		createCollection(function() {
			getNextSequenceValue('cid', function(err, id) {
				thisCompId = id - 1;
				cidIterator(increment);
			});
		});
	} else {
		createCookieObj(function() {
			cookieIterate();
		});
	}

}
createCookieObj(function() {
	cookieIterate();
});

function insertData(myobj) {
	getNextSequenceValue('company', function(err, id) {
		myobj['_id'] = id;
		collection.insertOne(myobj);
		console.log(id + ' rec Inserted')
	});
}



function callback(error, response, html) {
	console.log('callback ', increment);
	var mainDataObj;
	var current = 0;
	if (!error && response.statusCode == 200) {
		var res = entities.decode(html);
		var $ = cheerio.load(html);
		var codeTags = $('code');

		function iterateCodeTags(i) {
			resObj = {};
			var newres = $(codeTags[i]).html();
			if (entities.decode(newres)) {
				nextres = entities.decode(newres);
				if (typeof(nextres) == "string") {
					if (safelyParseJSON(nextres) !== undefined) {
						var obj = JSON.parse(nextres);
						if (obj["data"]) {
							if (obj["data"]["description"]) {
								for (var index in keys) {
									if (index < 7) {
										if (keys[index] == 'cid') {
											resObj[keys[index]] = Number(obj["data"][vals[index]].substr(33));
										} else if (keys[index] == 'empSizeInfo') {
											var infoObj = {
												'on': getTodayDateLong(),
												'size': (obj["data"][vals[index]])
											};
											resObj[keys[index]] = [infoObj];
										} else {
											if (keys[index] == '_id') {
												resObj[keys[index]] = compCount++;
											} else {
												resObj[keys[index]] = obj["data"][vals[index]];
											}
										}

									} else {
										if (typeof(vals[index]) == 'object') {
											var newObj = {};
											for (var currIndex in vals[index]) {
												newObj[locArr[currIndex]] = obj["included"][arrIndex[current]][vals[index][currIndex]];
											}
											resObj[keys[index]] = newObj;
											current++;
										} else {
											for (var val in obj["included"]) {
												if (typeof(obj["included"][val][
														[vals[index]]
													]) !== 'undefined') {
													if (obj["included"][val]['entityUrn'].includes(resObj['cid'].toString())) {
														resObj[keys[index]] = obj["included"][val][
															[vals[index]]
														];
														break;
													}

												}

											}

										}

									}
								}


							}
						}
					}

				}

			}
			if (i < codeTags.length) {
				if (Object.keys(resObj).length !== 0) {
					resObj["ctdOn"] = getTodayDateLong();
					resObj["status"] = 0;
					insertData(resObj);
				}
				iterateCodeTags(i + 1);
			} else {
				return;
			}

		}
		iterateCodeTags(0);
		setTimeout(function() {
			getNextSequenceValue("cid", function(err, id) {
				thisCompId = id - 1;
				cidIterator(++increment);
			})
		}, 0)

	} else {
		console.log('********', error)
		cookieVal++;
		createCookieObj(function() {
			cookieIterate();
		});
	}
}


function cidIterator(increment) {
	var options = {
		url: 'https://www.linkedin.com/company-beta/' + (thisCompId) + '/',
		headers: header,
		jar: true
	};
	if (increment <= fixedReqCount) {
		reqCount++;
		request(options, callback);

	} else if (cookieVal < cookieLimit) {
		cookieVal++;
		// cookieCollection.findAndModify({
		// 	cookies: thisCookie
		// }, {}, {
		// 	$set: {
		// 		"req": reqCount
		// 	}
		// });
		createCookieObj(function() {
			cookieIterate();
		});
	} else {
		console.log('here im');
		return;
	}
}