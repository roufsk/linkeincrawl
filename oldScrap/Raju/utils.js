var methods = {};
var config = require('./config.json');
MongoClient = require('mongodb').MongoClient;


methods.safelyParse = function safelyParseJSON(json) {
	var parsed;
	try {
		parsed = JSON.parse(json)
	} catch (e) {}
	return parsed
}

//Assigning request Count for each cookie we've used

methods.setReqCount = function setReqCount(thisCookieId, reqCount, cb) {
	console.log('******* ' + reqCount + ' Done for this cookie*******');
	cookieCollection.findAndModify({
		_id: thisCookieId
	}, {}, {
		$set: {
			"req": reqCount
		}
	}, function(err, item) {
		cb();
	});

}

methods.unsetCookie = function unsetCookie(thisCookieId, cb) {
	cookieCollection.findAndModify({
		"_id": thisCookieId
	}, {}, {
		$unset: {
			"timestamp": ""
		},
		$unset: {
			"cookies": ""
		}
	}, function(err, res) {
		cb();
	});
}

methods.getCookieVal = function getCookieVal(cb) {
	cookieCollection.findAndModify({
		// "_id": "roufs42@gmail.com"
		timestamp: null,
		cookies: {
			$ne: {},
			$ne: null
		}
	}, {}, {
		$set: {
			timestamp: methods.getTodayDateLong()
		}
	}, function(err, item) {
		if (item.value == null) {
			return (function() {
				console.error('No cookies are availabe to use');
				process.exit();
			})();
		} else {
			var thisCookie = item["value"]["cookies"];
			var thisCookieId = item["value"]["_id"];
			var reqCount = item["value"]["req"] || 0;
			console.log(thisCookieId, "thisCookieId");
			var resCookie = null;
			if (thisCookie && Object.keys(thisCookie).length !== 0) {
				resCookie = 'JSESSIONID="' + thisCookie.JSESSIONID + '";bcookie="' + thisCookie.bcookie + '";bscookie="' + thisCookie.bscookie + '";lang="' + thisCookie.lang + '";li_at="' + thisCookie.li_at + '";liap="' + thisCookie.liap + '";lidc="' + thisCookie.lidc + '";_lipt="' + thisCookie._lipt + '";visit="' + thisCookie.visit + '";sdsc="' + thisCookie.sdsc + '";sl="' + thisCookie.sl + '";_ga="' + thisCookie._ga + '"';
			}
			cb([thisCookieId, reqCount, resCookie, thisCookie.JSESSIONID]);
		}
	});
}

methods.getTodayDateLong = function getTodayDateLong() {
	var date = new Date().toISOString().substr(0, 10);
	return Math.floor((new Date(date + " 00:00:00")).getTime() / 1000);
};
methods.createCollection = function createCollection(callback) {
	MongoClient.connect(serverUrl, function(err, db) {
		if (err) throw err;
		inputCollection = db.collection("data1_to_be_deleted");
		linkedinrich = db.collection("linkedin_rich");
		test = db.collection("linkedin_rich");
		// overallStats = db.collection("crawl_stats_overall");
		MongoClient.connect(cookieUrl, function(err, cdb) {
			if (err) throw err;
			cookieCollection = cdb.collection("cookies");
			callback();
		})
	});
}
methods.logger = require('tracer').console({
	format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
	dateformat: "dd-mm-yyyy HH:MM:ss TT"
});
// url = "mongodb://" + config.server.username + ":" + config.server.password + "@" + config.server.ip + ":" + config.server.port + "/" + config.server.which_db + "?authSource=admin";
methods.header = config.headers;
methods.request = require('request');
methods.cheerio = require('cheerio');
methods.Encoder = require('html-entities');
methods.Entities = require('html-entities').AllHtmlEntities;
exports.data = methods;