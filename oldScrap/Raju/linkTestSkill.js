var fixedReqCount = 50,
	cookieLimit = 20,
	reqCount;
var utils = require('./utils.js'),
	sleep = require('system-sleep');
var header = utils.data.header,
	request = utils.data.request,
	cheerio = utils.data.cheerio,
	Encoder = utils.data.Encoder,
	Entities = utils.data.Entities;
var getCookieVal = utils.data.getCookieVal,
	unsetCookie = utils.data.unsetCookie,
	setReqCount = utils.data.setReqCount,
	safelyParseJSON = utils.data.safelyParse,
	getTodayDateLong = utils.data.getTodayDateLong,
	createCollection = utils.data.createCollection,
	logger = utils.data.logger,
	isEqual = utils.data.isEqual;
var entities = new Entities();
var config = require('./config.json');
serverUrl = "mongodb://" + config.server.username + ":" + config.server.password + "@" + config.server.ip + ":" + config.server.port + "/" + config.server.which_db + "?authSource=admin";
cookieUrl = "mongodb://" + config.cookieDB.username + ":" + config.cookieDB.password + "@" + config.cookieDB.ip + ":" + config.cookieDB.port + "/" + config.cookieDB.which_db + "?authSource=admin";
var callsArr = ['featuredSkills?includeHiddenEndorsers=true&count=50', 'profileContactInfo', 'following?count=7&q=followedEntities', 'recommendations?q=given', 'recommendations?q=received&recommendationStatuses=List(VISIBLE)']
var finalObj, skills, skillNames, accomplishments, recommendationsGiven, recommendationsRecieved, contactInfo, interests, finalInterests, volunteerExperience, experience, education;
var increment = 0,
	cookieVal = 0,
	expired = false;
var keys = ["firstName", "lastName", "occupation", "publicIdentifier"];
var input, mainOptions;
var resArr = [];

function unsetInputDone() {
	inputCollection.findAndModify({
			"result.publicProfileUrl": input
		}, {
			$unset: {
				"done": ""
			}
		},
		function() {
			process.exit();
		})
}

function exitHandler(options, err) {
	if (options.cleanup) {
		console.log("Process is done");
	}
	if (options.err) {
		if (reqBegin) {
			unsetInputDone();
		}
		expired = true;
		console.log("Error****", err);
	}
	if (options.exit) {
		if (reqBegin) {
			unsetInputDone();
		}

	}
}
process.on('exit', exitHandler.bind(null, {
	cleanup: true
}));
process.on('SIGINT', exitHandler.bind(null, {
	exit: true
}));

function checkExists(thisObj, thisItem) {
	if (Object.prototype.toString.call(thisObj[thisItem]) === '[object Array]') {
		if (thisObj[thisItem].length === 0)
			delete thisObj[thisItem];
	} else if (Object.prototype.toString.call(thisObj[thisItem]) === '[object Object]') {
		if (Object.keys(thisObj[thisItem]).length === 0)
			delete thisObj[thisItem];
		else {
			Object.keys(thisObj[thisItem]).forEach(function(currItem) {
				checkExists(thisObj[thisItem], currItem);
			})
		}
	}
}

function getNewCookie() {
	cookieVal++;
	increment = 0;
	resArr = [];
	getCookieVal(function(cookieArr) {
		thisCookieId = cookieArr[0];
		reqCount = cookieArr[1];
		header["cookie"] = cookieArr[2];
		header["csrf-token"] = cookieArr[3];
		cookieIterate();
	});
}

function cookieIterate() {
	if (header["cookie"] && (increment < fixedReqCount)) {
		if (expired) {
			expired = false;
			request(mainOptions, cb);
		} else {
			inputCollection.findAndModify({
					done: {
						$exists: false
					},
					"result.publicProfileUrl": {
						$exists: true
					}
				}, [
					['_id', 'asc']
				], {
					$set: {
						done: 1
					}
				},
				function(err, res) {
					reqBegin = 1;
					input = res.value.result.publicProfileUrl.split('/in/')[1];
					console.log(input);
					if (input) {
						mainOptions = {
							url: 'https://www.linkedin.com/in/' + input,
							headers: header
						};
						increment++;
						resArr = [],
							experience = [],
							education = [],
							skills = [],
							skillNames = [],
							volunteerExperience = [],
							accomplishments = {
								"honor": [],
								"course": [],
								"project": [],
								"language": [],
								"certification": [],
								"organization": [],
								"patent": [],
								"publication": [],
								"testScore": []
							},
							contactInfo = {},
							interests = {},
							finalInterests = [],
							contactInfo = {},
							recommendationsGiven = {},
							recommendationsRecieved = {},
							finalObj = {};
						request(mainOptions, cb);
						callsArr.forEach(function(item) {
							includedHandles(item);
						})
						reqCount++;
					} else {
						console.log("Not a valid input");
					}


				})
		}


	} else if (cookieVal < cookieLimit) {
		setReqCount(thisCookieId, reqCount, function() {
			getNewCookie();
		})
	} else {
		console.log("Process is done");
		setReqCount(thisCookieId, reqCount, function() {
			process.exit();
		});
	}

}

function callback(err, res, body) {
	if (!err && res.statusCode === 200) {
		if (safelyParseJSON(body) !== undefined) {
			var obj = safelyParseJSON(body);
			if (obj.data && (obj.data.metadata && obj.data.metadata.vieweeEndorsementsEnabled)) {
				obj.included.forEach(function(item) {
					if (item["entityUrn"] && item["entityUrn"].includes("fs_skill"))
						skillNames.push(item["name"]);
				})
				finalObj["skillNames"] = skillNames;
			} else if (res.request.uri.href.includes('recommendations?q=given')) {
				var recommenderDetails;
				obj.included.forEach(function(item) {
					if (item["recommender"]) {
						thisProfileId = item["recommender"];
						recommendationsGiven[item["recommendee"]] = {
							"recommedationText": item["recommendationText"],
							"recommenderDetails": recommenderDetails || {}
						}
					} else if (item["entityUrn"] && (item["entityUrn"] == thisProfileId)) {
						keys.forEach(function(currItem) {
							for (var key in recommendationsGiven) {
								recommendationsGiven[key]["recommenderDetails"][currItem] = item[currItem];
							}
							recommenderDetails = recommendationsGiven;

						})
					}
				})
				finalObj["recommendationsGiven"] = recommendationsGiven;
			} else if (res.request.uri.href.includes('recommendations?q=received&recommendationStatuses=List(VISIBLE)')) {
				obj.included.forEach(function(item) {
					if (item["recommender"]) {
						recommendationsRecieved[item["recommender"]] = {
							"recommedationText": item["recommendationText"],
							"recommenderDetails": {}
						}
					} else if (item["entityUrn"]) {
						for (var key in recommendationsRecieved) {
							if (key == item["entityUrn"]) {
								keys.forEach(function(currItem) {
									recommendationsRecieved[key]["recommenderDetails"][currItem] = item[currItem];
								})
							}
						}

					}
				});
				finalObj["recommendationsRecieved"] = recommendationsRecieved;
			} else if (res.request.uri.href.includes('profileContactInfo')) {
				var dobObj = {};
				if (obj["data"]["emailAddress"]) {
					contactInfo["email"] = obj["data"]["emailAddress"]
				}
				obj.included.forEach(function(item) {
					if (item["$id"] && item["$id"].includes("birthDateOn")) {
						["day", "month", "year"].forEach(function(thisItem) {
							dobObj[thisItem] = item[thisItem] || 0;
						})
						contactInfo["dob"] = dobObj;
					} else if (item["$type"].includes("ProfileWebsite")) {
						contactInfo["blog"] = item["url"];
					} else if (item["$type"].includes("TwitterHandle")) {
						contactInfo["twitterId"] = item["name"];
					} else if (item["$type"].includes("profile.IM")) {
						contactInfo["gtalk"] = item["id"];
					}
				})
				finalObj["contactInfo"] = contactInfo;
				// console.log(contactInfo);
			} else {
				obj.included.forEach(function(item) {
					if (item["entityUrn"] && !item["$type"].includes("FollowingInfo")) {
						["logo", "entityUrn", "$deletedFields", "trackingId", "showcase", "$type", "$id", "picture", "backgroundImage"].forEach(function(key) {
							if (item[key])
								delete item[key]
						})
						interests["urn:li:fs_followingInfo:" + item["objectUrn"]] = item;
					}
				})
				obj.included.forEach(function(item) {

					if (item["entityUrn"] && item["$type"].includes("FollowingInfo")) {
						["$deletedFields", "$type", "$id", "picture", "backgroundImage"].forEach(function(key) {
							if (item[key])
								delete item[key]
						})
						if (interests[item["entityUrn"]])
							interests[item["entityUrn"]]["followingDetails"] = item
					}
				})
				Object.keys(interests).forEach(function(item) {
					finalInterests.push(interests[item]);
				})
				finalObj["interests"] = finalInterests;

			}
			resArr.push(res.statusCode);
			if (resArr.length === callsArr.length + 1) {
				insertExtctdObj(finalObj);
				setImmediate(function() {
					cookieIterate();
				})

			}
		}
	} else {
		unsetCookie(thisCookieId, function() {
			setReqCount(thisCookieId, reqCount, function() {
				getNewCookie();
			})
		})
		console.log("expired");
	}

}



function includedHandles(field) {
	options = {
		url: 'https://www.linkedin.com/voyager/api/identity/profiles/' + input + '/' + field,
		headers: header
	};
	request(options, callback);
}


function insertExtctdObj(finalObj) {
	var finalObjKeys = Object.keys(finalObj);
	finalObjKeys.forEach(function(item) {
		if (finalObjKeys.indexOf(item) === finalObjKeys.length - 1) {
			Object.keys(finalObj).forEach(function(item) {
				checkExists(finalObj, item);
			})
		}
		checkExists(finalObj, item);
	})
	reqBegin = 0;
	linkedinrich.update({
			"_id": input
		}, {
			$set: {
				"linkedin_rich_profile": finalObj
			}
		}, {
			upsert: true
		},
		function(err, res) {
			if (err) {
				console.log(err);
			} else {
				console.log('updated done');
			}

		});
}

function extractData(thisObj) {
	var fieldsArr = ["position", "education", "skill", "volunteerExperience", "honor", "certification", "project", "course", "language", "organization", "patent", "publication", "testScore"];
	var variableArr = [experience, education, skills, volunteerExperience];
	var siteStndrdUrl = thisObj["profile"];
	var deletedFieldsArr = ["company", "courses", "honors", "$deletedFields", "$type", "entityUrn", "occupation", "projects", "members", "fieldOfStudyUrn", "inventors", "authors"];
	thisObj["included"].forEach(function(currItem) {
		for (var i = 0; i < fieldsArr.length; i++) {
			if (currItem["entityUrn"] && currItem["entityUrn"].includes("fs_" + fieldsArr[i])) {
				deletedFieldsArr.forEach(function(thisItem) {
					if (currItem[thisItem])
						delete currItem[thisItem]
				})
				if (accomplishments[fieldsArr[i]]) {
					// currItem["occupation"] ? delete currItem["occupation"] : console.log('');
					accomplishments[[fieldsArr[i]]].push(currItem)
					break;
				} else {
					variableArr[i].push(currItem);
					break;
				}

			}
		}

	})
	finalObj["experience"] = experience;
	finalObj["education"] = education;
	finalObj["skills"] = skills;
	finalObj["accomplishments"] = accomplishments;
	finalObj["volunteerExperience"] = volunteerExperience;
	if (resArr.length === callsArr.length + 1) {
		insertExtctdObj(finalObj);
		setImmediate(function() {
			cookieIterate();
		})
	}
}

function cb(err, res, html) {
	if (!err && res.statusCode == 200) {
		var $ = cheerio.load(html);
		var codeTags = $('code');
		resArr.push(res.statusCode);
		Object.keys(codeTags).forEach(function(index) {
			var newres = $(codeTags[index]).html();
			if (entities.decode(newres)) {
				nextres = entities.decode(newres);
				if (typeof(nextres) == "string") {
					if (safelyParseJSON(nextres) !== undefined) {
						var obj = JSON.parse(nextres);
						if ((obj["data"] && obj["included"]) && obj["data"]["profile"]) {
							extractData(obj);
						}
					}
				}
			}
		})

	} else {
		expired = true;
		cookieVal--;
		console.log('********', err);
		unsetCookie(thisCookieId, function() {
			setReqCount(thisCookieId, reqCount, function() {
				getNewCookie();
			})
		})
	}
}



createCollection(function() {
	getNewCookie();

})