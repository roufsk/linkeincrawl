var utils = require('./utils.js');
var async = require('async');
var dayWiseStats = {};
var totalStats = {};
var getNextSequenceValue = utils.data.getNextSequence,
	createCollection = utils.data.createCollection,
	getTodayDateLong = utils.data.getTodayDateLong;
invalid = false;
var ttlStatsKeys = ["_id", "date", "sucsCmpCount", "totalEmpCount"];
var ttlStatsVals, ttlDayStatsVals;
var thisDate = getTodayDateLong();
url = utils.data.mainUrl;
cookieUrl = utils.data.statsUrl;
logger = utils.data.logger;

var prevDate = thisDate - 86400;
console.log(thisDate);
var otherStats = {};
var desWiseStats = {};
var empSizeStats = {};
var desKeys = ["CEO", "CXO", "COO", "HR", "Human", "Manager", "Entre", "Admin", "Operations", "Sales", "Marketing", "Business", "Director"];
var desVals = ["CEO", "CXO", "COO", "HR", "HR", "Manager", "Entrepreneur", "Administrator", "Operations", "Sales", "Marketing", "BusinessAdmin", "Director"];
var cmpQueries = [{
	key: "dayInCmp",
	query: {
		"ctdOn": prevDate,
		"loc.cy": "in"
	}
}, {
	key: "dayCmp",
	query: {
		"ctdOn": prevDate
	}
}, {
	key: "ttlInCmp",
	query: {
		"loc.cy": "in"
	}
}];
var desQueiries = [{
	key: "overallStats",
	query: {
		prfsn: {
			$regex: null
		}
	}
}, {
	key: "dailyStats",
	query: {
		prfsn: {
			$regex: null
		},
		ctdOn: prevDate
	}
}];

var sizeQueries = [{
	key: "ttlEmpSize",
	query: {
		"cid": {
			$ne: null
		}
	}
}, {
	key: "ttlInEmpSize",
	query: {
		"loc.cy": "in"
	}
}, {
	key: "dayInEmpSize",
	query: {
		"loc.cy": "in",
		"ctdOn": prevDate
	}
}, {
	key: "dayttlEmpSize",
	query: {
		"ctdOn": prevDate
	}
}];

function generateStats() {
	countersCollection.find({}).toArray(function(err, items) {
		if (err) throw err;
		var thisObj = {};
		items.forEach(function(currentItem) {
			thisObj[currentItem["_id"]] = currentItem["seq"];
		});
		cmpCollection.count({
			"loc.cy": "in"
		}, function(err, count) {
			console.log(count);
			getNextSequenceValue("overallStats", function(err, id) {
				ttlStatsVals = [id, thisDate, thisObj["company"], thisObj["empDes"]];
				ttlStatsKeys.forEach(function(currentItem, index) {
					totalStats[currentItem] = ttlStatsVals[index];
				})
				console.log("totalStats", totalStats);
			});
		});
		empSizeCal();
		desWiseStatsGen();
		dayCrawlStats();

		function empSizeCal() {
			async.each(sizeQueries, function(item, cb) {
				cmpCollection.aggregate({
					$match: item.query
				}, {
					$group: {
						_id: null,
						total: {
							$sum: "$empSize"
						}
					}
				}, {
					allowDiskUse: true
				}, function(err, result) {
					empSizeStats[item.key] = result[0].total;
					cb();
				})
			}, function(err) {
				if (!err)
					console.log(empSizeStats);
			})
		}


		function desWiseStatsGen() {
			desWiseStats["overallStats"] = {};
			desWiseStats["dailyStats"] = {};
			async.each(desQueiries, function(item, callback) {
				async.each(desKeys, function(index, cb) {
					item.query.prfsn["$regex"] = index;
					empCollection.count(item.query, function(err, count) {
						desWiseStats[item["key"]][index] = count;
						cb();
					})
				}, function(err) {
					if (!err)
						callback();
				})
			}, function(err) {
				console.log(desWiseStats);
			})



		}

		function dayCrawlStats() {
			otherStats["dayWiseStats"] = {};
			async.parallel([
					function(cb) {
						requestStats.findOne({
							_id: prevDate
						}, function(err, item) {
							console.log(item);
							if (!err)
								otherStats["dayWiseStats"]["ttlCmpReqCount"] = item["seq"];
							cb();
						})
					},
					function(cb) {
						async.each(cmpQueries, function(result, cb) {
							cmpCollection.count(result.query, function(err, count) {
								if (!err)
									otherStats["dayWiseStats"][result.key] = count;
								cb();
							})
						}, function(err) {
							cb();
						});
					}
				],
				function(err) {
					console.log(otherStats);
				})



		}

	});
}



createCollection(function() {
	generateStats();
});