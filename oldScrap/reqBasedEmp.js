var countries = require('./countries.json'),
	utils = require('./utils.js'),
	sleep = require('system-sleep');
var headers = utils.data.header,
	request = utils.data.request,
	cheerio = utils.data.cheerio,
	Encoder = utils.data.Encoder,
	Entities = utils.data.Entities,
	logger = utils.data.logger,
	getCookieVal = utils.data.getCookieVal,
	unsetCookie = utils.data.unsetCookie,
	getNextSequenceValue = utils.data.getNextSequence,
	safelyParseJSON = utils.data.safelyParse,
	getTodayDateLong = utils.data.getTodayDateLong,
	createCollection = utils.data.createCollection,
	setReqCount = utils.data.setReqCount;
var entities = new Entities();
var cid, reqCount = 0,
	thisReqCount = 0,
	errrCount = 0;
//var reqLimit = Number(process.argv[2]),
//	cookieLimit = Number(process.argv[3]);
var reqLimit = 200,
	cookieLimit = 200;
var code,
	option, thisCookieId;
var cidArr = [],
	index = 0,
	cidBegin = false,
	lastCid = 0,
	designationCount = 0,
	lidCheck = 0,
	cookieVal = 0,
	substringCount = 0,
	cidPicked = false;
var reqPositionArr = ["Management", "Human", "Business", "Entrepreneurship", "Administrative", "Operations", "Sales", "Marketing"];
var thisCmpLidArr = [];
// MongoClient = require('mongodb').MongoClient;
// url = "mongodb://" + config.server.username + ":" + config.server.password + "@" + config.server.ip + ":" + config.server.port + "/" + config.server.which_db + "?authSource=admin";
// cookieUrl = "mongodb://" + config.cookieDB.username + ":" + config.cookieDB.password + "@" + config.cookieDB.ip + ":" + config.cookieDB.port + "/" + config.cookieDB.which_db + "?authSource=admin";
invalid = false;
url = utils.data.mainUrl;
cookieUrl = utils.data.statsUrl;

function exitHandler(options, err) {
	console.log("**************#########", options);
	if (options.cleanup) {
		logger.log("*********Everything is Done*******");
	}
	if (options.err)
		logger.log("*******Error is this ", err);
	if (options.exit) {
		if (cidPicked) {
			setReqCount(thisCookieId, (reqCount + thisReqCount), function() {
				getNextSequenceValue("incmpltCmps", function(err, id) {
					if (err) throw err;
					incmpltCmps.insertOne({
						"_id": id,
						"cid": cid,
						"index": index,
						"start": start,
					}, function(err, res) {
						process.exit();
					});
				})
			})
		}
	}
}

process.on('exit', exitHandler.bind(null, {
	cleanup: true
}));
process.on('SIGINT', exitHandler.bind(null, {
	exit: true
}));
//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {
	err: true,
	exit: true
}));

function clearCookie() {
	errrCount++;
	cookieVal--;
	unsetCookie(thisCookieId, function() {
		setReqCount(thisCookieId, (reqCount + thisReqCount), function() {
			cookieIterator();
		})
	});
}

function thisCmpValidation(myobj) {
	if (thisCmpLidArr.indexOf(myobj["lid"]) == -1) {
		getNextSequenceValue('empDes', function(err, id) {
			myobj['_id'] = id;
			empCollection.insertOne(myobj, function(err, res) {
				logger.log(id + ' rec Inserted');
				thisCmpLidArr.push(myobj["lid"]);
			});
			// cb();
		});
	}
}

function insertIntoDb(myobj) {
	if (lidCheck) {
		empCollection.findOne({
			"lid": myobj["lid"]
		}, function(err, item) {
			if (item == null)
				thisCmpValidation(myobj);
		})
	} else {
		thisCmpValidation(myobj);
	}

}

// function dataValidation(myobj) {
// 	// if (cidBegin == true) {
// 	if (thisCmpLidArr.indexOf(myobj["lid"] == -1)) {
// 		insertIntoDb(myobj, function(err, res) {
// 			thisCmpLidArr.push(myobj["lid"]);
// 		});
// 	}
// 	// empCollection.find({
// 	// 	"lid": myobj["lid"]
// 	// }).toArray(function(err, item) {
// 	// 	if (item.length == 0) {
// 	// 		insertIntoDb(myobj);
// 	// 	}
// 	// })
// }

function getCidVal(callback) {
	getNextSequenceValue('prcd', function(err, id) {
		cmpCollection.findOne({
			"_id": id
		}, function(err, item) {
			if (item == null) throw ("No Indian Companies existed")
			cid = item["cid"];
			if (item["loc"]["cy"] == "in") {
				cmpCollection.find({
					"name": item["name"],
					"type": item["type"]
				}).toArray(function(err, items) {
					if (items.length > 1)
						lidCheck = 1;
					option = {
						url: 'https://www.linkedin.com/edu/alumni-by-school?id=&name=university+OR+universite+OR+universidad+OR+universiteit+OR+universitat+OR+universitas+OR+universita+OR+college+OR+hochschule+OR+hogeschool+OR+colegio+OR+school+OR+ecole+OR+escuela+OR+ecola+OR+academy+OR+institute+OR+%E5%A4%A7%E5%AD%A6+OR+%E5%AD%A6%E9%99%A2+OR+%D1%83%D0%BD%D0%B8%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%82%D0%B5%D1%82+OR+instituto+OR+institut&facets=CN%2E22%2CCC%2E' + cid + '&startYear=&endYear=&dateType=attended&incNoDates=true&keyword=&start=0&count=10&hideSchoolAsEmployer=',
						headers: headers
					};
					// logger.log(cid);
					cidArr.push(cid);
					callback();

				})

			} else {
				getCidVal(function() {
					cidPicked = true;
					request(option, desArrCreator);
					reqCount++;
				});
			}

		});
	});
}

function cidIterator() {
	// logger.log(companyCount)
	if (cidBegin) {
		desIterator(index);
	} else {
		// if (reqCount < reqLimit) {
		getCidVal(function() {
			thisCmpLidArr = [];
			cidPicked = true;
			request(option, desArrCreator);
			reqCount++;
		});
	}

}

function getNewCookie() {
	thisReqCount = 0;
	getCookieVal(function(cookieArr) {
		thisCookieId = cookieArr[0];
		reqCount = cookieArr[1];
		headers["cookie"] = cookieArr[2];
		cidIterator();
	});
}

function cookieIterator() {
	if (index < designationCount)
		cidBegin = true;

	if (cookieVal < cookieLimit) {
		getNewCookie();
		cookieVal++;
	} else {
		logger.log(index < designationCount, "*****cidBegin");
		if (index < designationCount) {
			lastCid = 1;
			getNewCookie();
			logger.log("Im hereee baby")
			sleep(2000);

		} else {
			logger.log(cidArr, errrCount, substringCount);
			process.exit();
		}
	}
}

function objIterator(i) {
	var obj = {};
	if (values[i]) {
		var loc = [];
		var prfsn = values[i]["headline"];
		var str = values[i]["publicProfileUrl"].split("=")[1].split("&authType")[0];
		if (str.substring(0, 5) == "ABAAA") {
			str = str.substring(5, str.length)
		} else {
			substringCount++;
		}
		if (values[i]["location"]) {
			loc = values[i]["location"].split(", ");
			var locName = loc[loc.length - 1];
			var locInCaps = locName[0].toUpperCase() + locName.substring(1, locName.length);
			if (loc.length == 3) {
				var temp = loc[2];
				loc[2] = loc[1];
				loc[1] = temp;
			} else if (loc.length == 1) {
				if (countries[locInCaps] != undefined) {
					loc.push(loc[0]);
					loc[0] = "";
				}
			} else {
				if (countries[locInCaps] == undefined) {
					loc.push(locInCaps);
					loc[1] = "";
				}
			}
		}

		if (prfsn) {
			if (prfsn.includes(" at ")) {
				prfsn = prfsn.split(" at ")[0];
			}
		}
		obj["lid"] = str;
		obj["name"] = values[i]["fullName"];
		obj["picUrl"] = values[i]["fullPictureURL"] || "";
		obj["cid"] = cid;
		obj["loc"] = {
			"ct": loc[0] !== undefined ? ((loc[0].split(' Area')[0]) || "") : "",
			"al": loc[2] !== undefined ? ((loc[2].split(' Area')[0]) || "") : "",
			"cy": loc[1] !== undefined ? (loc[1].split(' Area')[0] || "") : ""
		};
		obj["prfsn"] = prfsn;
		obj["ctdOn"] = getTodayDateLong();
		insertIntoDb(obj);
		// setImmediate(function() {
		objIterator(i + 1);
		// });


	} else {
		return;
	};
}

function desArrCreator(error, response, body) {
	if (!error && response.statusCode == 200) {
		index = 0;
		logger.log('cb***', cid);
		var object = safelyParseJSON(body);
		if (object == undefined) {
			invalid = true;
			clearCookie();
		} else {
			designationArr = object["content"]["alumniBySchool"]["facets"]["2"]["buckets"];
			reqDesignationArr = [];
			designationArr.forEach(function(currentVal) {
				reqPositionArr.forEach(function(val) {
					if (currentVal["name"].includes(val)) {
						reqDesignationArr.push(currentVal);
					}
				})
			})
			designationCount = reqDesignationArr.length;
			// logger.log('here**', reqDesignationArr);
			desIterator(index);
		}

	} else {
		logger.log("im herrrrrrrrr");
		invalid = true;
		clearCookie();
	}
}

function desIterator(index) {
	logger.log(cidBegin);
	if (cidBegin == false)
		start = 0;
	logger.log("**********index", index);
	if (index < designationCount) {
		code = reqDesignationArr[index]['code'];
		limit = reqDesignationArr[index]['count'];
		limit = limit < 100 ? limit : 100;
		countIterator(start, code, limit);
	} else {
		if (lastCid) {
			setReqCount(thisCookieId, (reqCount + thisReqCount), function() {
				logger.log("***Done for All");
				process.exit();
			})
		} else {
			cidBegin = false;
			cidPicked = false;
			cidIterator();
		}

	}
}

function callback(error, response, body) {
	if (!error && response.statusCode == 200) {
		i = 0;
		var resObj = safelyParseJSON(body);
		if (resObj == undefined) {
			cidBegin = true;
			clearCookie();
		} else {
			values = resObj["content"]["alumniBySchool"]["people"]["values"];
			objIterator(i);
			start += 10;
			countIterator(start, code, limit);
		}
	} else {
		cidBegin = true;
		clearCookie();
	}
}

function countIterator(start, code, limit) {
	logger.log(start, code);
	var options = {
		url: 'https://www.linkedin.com/edu/alumni-by-school?id=&name=university+OR+universite+OR+universidad+OR+universiteit+OR+universitat+OR+universitas+OR+universita+OR+college+OR+hochschule+OR+hogeschool+OR+colegio+OR+school+OR+ecole+OR+escuela+OR+ecola+OR+academy+OR+institute+OR+%E5%A4%A7%E5%AD%A6+OR+%E5%AD%A6%E9%99%A2+OR+%D1%83%D0%BD%D0%B8%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%82%D0%B5%D1%82+OR+instituto+OR+institut&facets=CN%2E' + code + '%2CCC%2E' + cid + '&startYear=&endYear=&dateType=attended&incNoDates=true&keyword=&start=' + start + '&count=10&hideSchoolAsEmployer=',
		headers: headers
	};
	if (thisReqCount < reqLimit) {
		if (start < limit) {
			thisReqCount++;
			request(options, callback);
		} else {
			cidBegin = false;
			desIterator(++index);
		};
	} else {
		setReqCount(thisCookieId, (reqCount + thisReqCount), function() {
			cookieIterator();
		})
	}

}
try {
	createCollection(function() {
		cookieIterator();
	})
} catch (err) {
	logger.log('errrrrrrrrrrrrrrrrrr', err);
}