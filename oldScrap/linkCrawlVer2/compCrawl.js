	var utils = require('./utils.js'),
		sleep = require('system-sleep');
	var header = utils.data.header,
		request = utils.data.request,
		cheerio = utils.data.cheerio,
		Encoder = utils.data.Encoder,
		Entities = utils.data.Entities;
	var getCookieVal = utils.data.getCookieVal,
		unsetCookie = utils.data.unsetCookie,
		setReqCount = utils.data.setReqCount,
		getNextSequenceValue = utils.data.getNextSequence,
		safelyParseJSON = utils.data.safelyParse,
		getTodayDateLong = utils.data.getTodayDateLong,
		createCollection = utils.data.createCollection,
		logger = utils.data.logger,
		isEqual = utils.data.isEqual;
	var entities = new Entities();
	var fixedReqCount = Number(process.argv[2]),
		cookieLimit = Number(process.argv[3]);
	var thisCompId, thisCookieId, compCount, resObj, resCookie, options;
	var reqCount = 0,
		increment = 0,
		cookieVal = 1,
		errCount = 0,
		sleepBool = 0;
	var expired = false,
		validReqCount = 0,
		cidBegin = false;
	var locArr = ["ct", "cy", "pCode"];
	var keys = ["_id", "cid", "name", "type", "empSize", "empSizeInfo", "domain", "loc", "followers"];
	var vals = ["", "url", 'name', "industries", "staffCount", "staffCount",
		"companyPageUrl", ["geographicArea", "country", "postalCode"],
		"followerCount"
	];
	invalid = false;
	url = utils.data.mainUrl;
	cookieUrl = utils.data.statsUrl;
	//Terminating when no arguments has been passed
	if (!(fixedReqCount && cookieLimit))
		throw ("FixedReqCount and CookieLimit as cmd arguments should be mentioned")

	function exitHandler(options, err) {
		if (options.cleanup) {
			console.log("Process is done");
			console.log("Last cookieVal***** ", cookieVal);
			console.log("Total Err Count ", errCount);
			console.log("********totaltime*******");
			console.timeEnd("time");
			console.log("********totaltime*******");
			console.log("Toatal InvalidReqCount is ", ((fixedReqCount * cookieVal) - validReqCount))
			console.log(process.memoryUsage());
		}
		if (options.err)
			console.log("Err**", err);
		if (options.exit) {
			setReqCount(thisCookieId, reqCount, function() {
				if (cidBegin) {
					cmpCollection.findOne({
						"cid": thisCompId
					}, function(err, doc) {
						if (!doc) {
							getNextSequenceValue("unprcd", function(err, id) {
								if (err)
									throw err;
								unprcdCollection.insertOne({
									"_id": id,
									"cid": thisCompId
								}, function(err, res) {
									process.exit()
								});

							})
						} else {
							process.exit(1);
						}
					})

				} else {
					process.exit();
				}
			})

		}

	}
	process.on('exit', exitHandler.bind(null, {
		cleanup: true
	}));
	process.on('SIGINT', exitHandler.bind(null, {
		exit: true
	}));
	//catches uncaught exceptions
	process.on('uncaughtException', exitHandler.bind(null, {
		err: true,
		exit: true
	}));

	function getNewCookie() {
		getCookieVal(function(cookieArr) {
			thisCookieId = cookieArr[0];
			reqCount = cookieArr[1];
			header["cookie"] = cookieArr[2];
			cookieIterate();
		});
	}

	function cookieIterate() {
		if (header["cookie"]) {
			if (sleepBool)
				sleep(5000);
			sleepBool = 1;
			increment = 0;
			cidIterator(increment);
		} else {
			logger.log("**************Cookie is No More************");
			getNewCookie();
		}

	}

	function insertData(myobj) {
		cmpCollection.findOne({
			"name": myobj["name"],
			"domain": myobj["domain"]
		}, function(err, item) {
			if (item == null) {
				getNextSequenceValue('company', function(err, id) {
					myobj['_id'] = id;
					cmpCollection.insertOne(myobj, function() {
						cidBegin = false;
						console.log(id + ' rec Inserted');
					});
				});
			} else {
				myobj["_id"] = item["_id"];
				if (isEqual(item, myobj)) {
					logger.log("Already existed");
				} else {
					id = item["_id"];
					myobj["updtdOn"] = getTodayDateLong();
					console.log(id + ' rec Updated');
					cmpCollection.update({
						_id: id
					}, myobj);
				}
			}
		})

	}

	function buildData(obj, cid, entityUrn) {
		// console.log(obj);
		var current = 0,
			dataObj = {};
		var included = obj["included"];
		included.forEach(function(item) {
			if (item["entityUrn"] == entityUrn && (item["url"] == "https://www.linkedin.com/company/" + cid)) {
				dataObj["rawData"] = item;
			} else if (item["country"] || item["city"])
				dataObj["countryData"] = item;
			else if (item["entityUrn"] == 'urn:li:fs_followingInfo:urn:li:company:' + cid)
				dataObj["followerCount"] = item["followerCount"];

		})
		dataObj["rawData"] ? (dataObj["rawData"]["followerCount"] = dataObj["followerCount"]) : console.log("No data");
		if (dataObj["rawData"])
			for (var index in keys) {
				if (keys[index] == 'cid') {
					resObj[keys[index]] = Number(dataObj["rawData"][vals[index]].substring(33));
				} else if (keys[index] == 'empSizeInfo') {
					var infoObj = {
						'on': getTodayDateLong(),
						'size': (dataObj["rawData"][vals[index]]) || ""
					};
					resObj[keys[index]] = [infoObj];
				} else if (typeof(vals[index]) == 'object') {
					var newObj = {};
					for (var currIndex in vals[index]) {
						newObj[locArr[currIndex]] = dataObj["countryData"] ? (dataObj["countryData"][vals[index][currIndex]] || "") : "";
					}
					resObj[keys[index]] = newObj;
					current++;
				} else {
					resObj[keys[index]] = dataObj["rawData"][vals[index]] || "";
				}
			}

	}

	function iterateCodeTags(i, codeTags, cid) {
		resObj = {};
		var newres = $(codeTags[i]).html();
		if (entities.decode(newres)) {
			var nextres = entities.decode(newres);
			if (typeof(nextres) == "string") {
				if (safelyParseJSON(nextres) !== undefined) {
					var obj = JSON.parse(nextres);
					var entityUrnStr = 'urn:li:fs_normalized_company:' + cid;
					if (obj["data"] && (obj["data"]["included"] && obj["data"]["included"].length > 0)) {
						buildData(obj, cid, entityUrnStr);
					} else {
						// logger.log('else case');
					}
				}
			}

		}
		if (i < codeTags.length) {
			if (Object.keys(resObj).length !== 0 && resObjStatus === false) {
				resObj["ctdOn"] = getTodayDateLong();
				resObj["status"] = 0;
				validReqCount++;
				// console.log(resObj);
				insertData(resObj);
				resObjStatus = true;
			}
			iterateCodeTags((i + 1), codeTags, cid);
		} else {
			// return;
			// logger.log("Terminating the iterateCodeTags");
		}

	}
	//Process the response
	function callback(error, response, html) {
		console.log('callback ', increment);
		if (!error && response.statusCode == 200) {
			resObjStatus = false;
			var res = entities.decode(html);
			$ = cheerio.load(html);
			var codeTags = $('code');
			iterateCodeTags(0, codeTags, thisCompId);
			setImmediate(function() {
				// logger.log("returned iterateCodeTags function");
				cidIterator(++increment);
			});

		} else {
			errCount++;
			expired = true;
			cookieVal--;
			console.log('********', error);
			unsetCookie(thisCookieId, function() {
				setReqCount(thisCookieId, reqCount, function() {
					getNewCookie();
				})
			})


		}
	}


	//Iterate the company id wise
	function cidIterator(increment) {
		if (increment < fixedReqCount) {
			if (expired === false) {
				getNextSequenceValue("cid", function(err, id) {
					logger.log("cid picked****", id);
					cidBegin = true;
					thisCompId = id;
					options = {
						url: 'https://www.linkedin.com/company-beta/' + (thisCompId) + '/',
						headers: header,
						jar: true
					};
					request(options, callback);
				})
			} else {
				expired = false;
				request(options, callback);
			}
			reqCount++;

		} else if (cookieVal < cookieLimit) {
			cookieVal++;
			console.log('Coookieeeeee Val ', cookieVal);
			setReqCount(thisCookieId, reqCount, function() {
				getNewCookie();
			});
		} else {
			setReqCount(thisCookieId, reqCount, function() {
				process.exit();
			});
		}
	}

	//Main function Calling Starting of the Program
	console.time("time");
	createCollection(function() {
		getNewCookie();
	})