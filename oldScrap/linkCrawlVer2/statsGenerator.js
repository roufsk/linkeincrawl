var utils = require('./utils.js');
var dayWiseStats = {};
var totalStats = {};
var getNextSequenceValue = utils.data.getNextSequence,
	createCollection = utils.data.createCollection,
	getTodayDateLong = utils.data.getTodayDateLong;
invalid = false;
var ttlStatsKeys = ["_id", "date", "ttlCmpReqCount", "sucsCmpCount", "failCmpReqCount", "totalEmpCount"];
var ttlStatsVals, ttlDayStatsVals;
var thisDate = getTodayDateLong();
url = utils.data.mainUrl;
cookieUrl = utils.data.statsUrl;
logger = utils.data.logger;

function generateStats() {
	countersCollection.find({}).toArray(function(err, items) {
		if (err) throw err;
		var thisObj = {};
		items.forEach(function(currentItem) {
			thisObj[currentItem["_id"]] = currentItem["seq"];
		});
		overallStats.findOne({
			"date": thisDate
		}, function(err, item) {
			if (item === null) {
				getNextSequenceValue("overallStats", function(err, id) {
					ttlStatsVals = [id, thisDate, thisObj["cid"], thisObj["company"], (thisObj["cid"] - thisObj["company"]), thisObj["empDes"]];
					ttlStatsKeys.forEach(function(currentItem, index) {
						totalStats[currentItem] = ttlStatsVals[index];
					})
					console.log("totalStats", totalStats);
					overallStats.insertOne(totalStats, function(err, res) {
						overallStats.findOne({
							"_id": id - 1
						}, function(err, item) {
							if (item !== null) {
								getNextSequenceValue("dayWiseStats", function(err, id) {
									ttlStatsVals[0] = id;
									ttlStatsVals[2] = totalStats["ttlCmpReqCount"] - item["ttlCmpReqCount"];
									ttlStatsVals[3] = totalStats["sucsCmpCount"] - item["sucsCmpCount"];
									ttlStatsVals[4] = totalStats["failCmpReqCount"] - item["failCmpReqCount"];
									ttlStatsVals[5] = totalStats["totalEmpCount"] - (item["totalEmpCount"] || 0);

									ttlStatsKeys.forEach(function(currentItem, index) {
										dayWiseStats[currentItem] = ttlStatsVals[index];
									})
									dailyStats.insertOne(dayWiseStats);
									logger.log("dayWiseStats", dayWiseStats);
								})
							}
						})

					});
					logger.log("overallStats======", totalStats);
				})
			} else {
				logger.log("overallStats=====", item);
			}
		})


	})
}

createCollection(function() {
	generateStats();
});