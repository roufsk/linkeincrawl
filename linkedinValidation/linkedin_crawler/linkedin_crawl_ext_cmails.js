var request = require('request');
var async = require('async');
// var aes = require('./aes');

var headers = {
	'cookie': 'bcookie="v=2&c0fa3f63-93a3-4938-8580-4a720c7f9b46"; lang="v=2&lang=en-us"; _ga=GA1.2.237324447.1491564729; sdsc=1%3A1SZM1shxDNbLt36wZwCgPgvN58iw%3D; liap=true; _lipt=CwEAAAFcfLA9DueWimkSfYqgI6LE4tKJ_LsMIuHQl9z9ckQOVkKhC6HnAnrZenPUbPXhpIo8AThXk6YVZsj2tSw9QCeKGt27k-hqsf_weiIu7a3S2zEgn46hrcMZUO-f_NDHNIflMQTEMPOvxmkeDblxDvo-ih9p43MWSLsyvtp6We-RRYPYZW0OzSQWqVeNM5SmXkJl0jNWL1DFl8pNl_avSEVL6Yp5xOoI8iTR66rxmziETRKFl3dA; lidc="b=SGST07:g=2:u=1:i=1496740595:t=1496826995:s=AQGObFX3KV_NDinmH1U6Xb5syKJi_goA"',
	'accept-language': 'en-GB,en-US;q=0.8,en;q=0.6',
	'x-requested-with': 'IN.XDCall',
	'x-li-format': 'json',
	'x-http-method-override': 'GET',
	'x-cross-domain-origin': 'https://mail.google.com',
	'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/58.0.3029.110 Chrome/58.0.3029.110 Safari/537.36',
	'content-type': 'application/json',
	'accept': '*/*',
	'referer': 'https://api.linkedin.com/uas/js/xdrpc.html?v=0.0.2000-RC8.61108-1429',
	'authority': 'api.linkedin.com'
};

var settings = require('./config.json');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var BULK_COUNT = 100 * 4;
var MAX_COUNT = 20 * 1;

// Use connect method to connect to the Server
try {
	MongoClient.connect(settings.linkedinTokenURL, function(err, db) {
		assert.equal(null, err);
		db.admin().authenticate(settings.linkedinTokenUsername, settings.linkedinTokenPassword, function(err, res) {
			if (err) {
				console.error(err.stack);
				process.exit(0);
			} else {
				console.log("Connected correctly to server");
				var tokens = db.collection(settings.tokens);
				var timewise_stats = db.collection('timewise_stats');

				MongoClient.connect(settings.linkedinDBURL, function(err, db) {
					assert.equal(null, err);
					console.log("Connected correctly to server");
					var linkedin_trail = db.collection(settings.data_cm);
					var timeWiseStats = db.collection("timewise_stats");
					var validCounter = db.collection("counters");
					var matchedProfiles = db.collection("matched_profiles");
					var predictions2 = db.collection("predictions2");
					var dailyStats = db.collection("crawl_stats_daily");
					var overallStats = db.collection("crawl_stats_overall");

					function statsInc(statsColl, field, type) {
						var id = "ttlStats"
						if (type == "daily")
							id = getTodayDateLong();

						statsColl.update({
							_id: id
						}, {
							$inc: field
						}, {
							new: true,
							upsert: true
						}, function(err, res) {
							// if (!err)
							// console.log("done");
						});
					};

					function getTodayDateLong() {
						var date = new Date().toISOString().substr(0, 10);
						return Math.floor((new Date(date + " 00:00:00")).getTime() / 1000);
					};

					function timewiseStats(distance, distance_1, distance_2, distance_3, no_member, linkedin_true, hold, no_access, invalid_request, internal_error, token_expired, throttles, distance_0, distance_minus1, no_distance, other_result, succ_search, cb) {
						var mhd = getMHD(new Date());
						var incDoc = {};
						if (linkedin_true > 0) incDoc['linkedin_true'] = linkedin_true;
						var member = distance + no_access;
						var total = no_member + member + invalid_request + hold;
						var hits = total + internal_error + token_expired + throttles + other_result;
						if (no_member > 0) incDoc["no_member"] = no_member;
						if (hold > 0) incDoc['hold'] = hold;
						if (distance_1 > 0) incDoc["distance_1"] = distance_1;
						if (distance_2 > 0) incDoc["distance_2"] = distance_2;
						if (distance_3 > 0) incDoc["distance_3"] = distance_3;
						if (distance_0 > 0) incDoc['distance_0'] = distance_0;
						if (distance_minus1 > 0) incDoc['distance_minus1'] = distance_minus1;
						if (no_distance > 0) incDoc['no_distance'] = no_distance;
						if (other_result > 0) incDoc['other_result'] = other_result;
						if (distance > 0) incDoc["distance"] = distance;
						if (no_access > 0) incDoc["no_access"] = no_access;
						if (invalid_request > 0) incDoc["invalid_request"] = invalid_request;
						if (member > 0) incDoc["member"] = member;
						if (total > 0) incDoc["total"] = total;
						if (internal_error > 0) incDoc["internal_error"] = internal_error;
						if (token_expired > 0) incDoc["token_expired"] = token_expired;
						if (throttles > 0) incDoc["throttles"] = throttles;
						if (hits > 0) incDoc["hits"] = hits;
						if (succ_search) incDoc["succ_attempts"] = 1;
						if (unique_attempts) incDoc["unique_attempts"] = 1;
						incDoc["attempts"] = 1;
						timeWiseStats.update({
							_id: mhd.d
						}, {
							$inc: incDoc,
							$set: {
								updated_on: parseInt(Date.now() / 1000)
							}
						}, {
							upsert: true
						}, function(err, ok) {
							if (err)
								cb(err);
							else {
								timewise_stats.update({
									_id: mhd.d
								}, {
									$inc: incDoc,
									$set: {
										updated_on: parseInt(Date.now() / 1000)
									}
								}, {
									upsert: true
								}, function(err, ok) {
									if (err) {
										cb(err);
									} else {
										cb(null, "ok");
									}
								});
							}


						});

					}

					function throttle_reached(res, count, throttles, token_expired, internal_error, no_access, invalid_request, no_member, linkedin_true, hold, distance_1, distance_2, distance_3, successfulSearch, distance_0, distance_minus1, no_distance, other_result, no_mails_to_crawl) {
						var incDoc = {
							searches: count
						};
						var setDoc = {};
						if (!no_mails_to_crawl) setDoc['use'] = 1;
						if (throttles > 0) incDoc['throttles'] = throttles;
						if (no_access > 0) incDoc['no_access'] = no_access;
						if (invalid_request > 0) incDoc['invalid_request'] = invalid_request;
						if (token_expired > 0) incDoc['token_expired'] = token_expired;
						if (internal_error > 0) incDoc['internal_error'] = internal_error;
						if (no_member > 0) incDoc['no_member'] = no_member;
						if (linkedin_true > 0) incDoc['linkedin_true'] = linkedin_true;
						if (hold > 0) incDoc['hold'] = hold;
						var distance = distance_1 + distance_2 + distance_3 + distance_0 + distance_minus1 + no_distance;
						if (distance > 0) incDoc['distance'] = distance;
						if (distance_1 > 0) incDoc['distance_1'] = distance_1;
						if (distance_2 > 0) incDoc['distance_2'] = distance_2;
						if (distance_3 > 0) incDoc['distance_3'] = distance_3;
						if (distance_0 > 0) incDoc['distance_0'] = distance_0;
						if (distance_minus1 > 0) incDoc['distance_minus1'] = distance_minus1;
						if (no_distance > 0) incDoc['no_distance'] = no_distance;
						if (other_result > 0) incDoc['other_result'] = other_result;
						if (successfulSearch) {
							setDoc['successful_search'] = parseInt(Date.now() / 1000);
							incDoc['succ_attempts'] = 1;
						}
						incDoc['attempts'] = 1;

						tokens.update({
							_id: res._id
						}, {
							$set: setDoc,
							$inc: incDoc
						}, function(err, ok) {
							if (err) {
								console.log(err);
							} else {
								console.log("use 1 updated");
								timewiseStats(distance, distance_1, distance_2, distance_3, no_member, linkedin_true, hold, no_access, invalid_request, internal_error, token_expired, throttles, distance_0, distance_minus1, no_distance, other_result, successfulSearch, function(err, ok) {
									if (err) {
										console.error(err.stack);
										process.exit(0);
									} else {
										// setTimeout(repeat, 0);
										setTimeout(function() {
											process.exit(0);
										}, 0);
									}
								});
							}
						});
					}

					function testPassed(token, cb) {
						linkedin_crawler({
							_id: "roufs42@gmail.com"
						}, token, function(error, data) {
							if (error) {
								console.error(error.stack);
								process.exit(0);
							} else {
								console.log(data.result);
								if (data.result.indexOf("Couldn't find member") > -1) {
									return cb(false);
								} else if (data.result.indexOf("Invalid HTTP Request") > -1) {
									console.log(data.result);
									process.exit(0);
								} else {
									return cb(true);
								}
							}
						});
					}

					function repeat() {
						unique_attempts = false;
						tokens.findAndModify({
							active: true,
							use: null,
							hit_time: {
								$gt: getMHD(new Date()).m - 2400
							}
						}, [
							['hit_time', 'asc']
						], {
							$set: {
								use: 0
							}
						}, function(err, res) {
							res = res.value;
							if (res) {
								var mhd = getMHD(new Date());
								if (res.old_hit_time < mhd.d) {
									unique_attempts = true;
								}
								console.log("--------------->", res, {
									$or: [{
										n: null
									}, {
										n: {
											$lte: 10
										},
										searched_by: {
											$ne: res.lid
										}
									}],
									c: null
								});
								linkedin_trail.find({
									$or: [{
										n: null
									}, {
										n: {
											$lte: 20
										},
										searched_by: {
											$ne: res.lid
										}
									}],
									c: null
								}).sort({
									n: 1
								}).limit(1).toArray(function(err, docs) {
									var length = docs.length;
									if (length === 0) {
										tokens.update({
											_id: res._id
										}, {
											$unset: {
												use: ""
											}
										}, function(err, ok) {
											if (err) {
												console.log(err);
											} else {
												console.log("use 1 unset!", {
													_id: res._id
												});
												console.log("no mails @ checking!");
												// setTimeout(repeat, 60000);
												setTimeout(function() {
													process.exit(0);
												}, 60000);
											}
										});
									} else {
										linkedin_crawler(docs[0], res.token, function(error, data) {
											if (error) {
												console.error(error.stack);
												process.exit(0);
											} else {
												if (data.result.indexOf("Throttle limit for calls to this resource is reached.") > -1) {
													throttle_reached(res, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, 0, 0, 0, 0, false);
												} else if (data.result.indexOf("token expired") > -1) {
													throttle_reached(res, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, 0, 0, 0, 0, false);
												} else {
													function bulk() {
														linkedin_trail.find({
															$or: [{
																n: null
															}, {
																n: {
																	$lte: 10
																},
																searched_by: {
																	$ne: res.lid
																}
															}],
															c: null
														}).sort({
															n: 1
														}).limit(BULK_COUNT).toArray(function(err, docs) {
															var length = docs.length;
															if (length === 0) {
																console.log("no mails!");
																if (throttleReached > 0) {
																	console.log("Throttle reached in the middle - " + count * BULK_COUNT + " : " + throttleReached);
																}
																if (token_expired > 0) {
																	console.log("Token expired in the middle - " + count * BULK_COUNT + " : " + token_expired);
																}
																throttle_reached(res, count * BULK_COUNT, throttleReached, token_expired, internal_error, no_access, invalid_request, linkedin_true, hold, no_member, distance_1, distance_2, distance_3, true, distance_0, distance_minus1, no_distance, other_result, true);
																// setTimeout(repeat, 60000);
																setTimeout(function() {
																	process.exit(0);
																}, 60000);
															} else {
																var batch_confident = linkedin_trail.initializeUnorderedBulkOp({
																	useLegacyOps: true
																});
																var batch_remaining = linkedin_trail.initializeUnorderedBulkOp({
																	useLegacyOps: true
																});
																var cone_counter = 0;
																async.each(docs, function(item, cb) {
																	linkedin_crawler(item, res.token, function(error, data) {
																		if (error) {
																			console.log("----error----");
																			cb(error);
																		} else {
																			if (data.result.indexOf("Throttle limit for calls to this resource is reached.") > -1) {
																				throttleReached += 1;
																				cb();
																			} else if (data.result.indexOf("Internal service error") > -1) {
																				internal_error += 1;
																				cb();
																			} else if (data.result.indexOf("token expired") > -1) {
																				token_expired += 1;
																				cb();
																			} else if (data.result.indexOf("[invalid.profile.access]. You don't have access to the profile") > -1) {
																				no_access += 1;
																				if (!data.searched_by)
																					linkedin_true += 1;
																				var set = {
																					linkedin: true,
																					updated_on: parseInt(Date.now() / 1000)
																				};
																				batch_confident.find({
																					_id: data._id
																				}).updateOne({
																					$set: set,
																					$addToSet: {
																						searched_by: res.lid
																					},
																					$inc: {
																						n: 1
																					}
																				});
																				cb();
																			} else if (data.result.indexOf("Couldn't find member") > -1) {
																				if (data.searched_by) {
																					hold += 1;
																					batch_remaining.find({
																						_id: data._id
																					}).updateOne({
																						$set: {
																							updated_on: parseInt(Date.now() / 1000)
																						},
																						$push: {
																							hold_case: res.lid
																						},
																						$inc: {
																							n: 1
																						}
																					});
																					cb();
																				} else {
																					no_member += 1;
																					batch_remaining.find({
																						_id: data._id
																					}).updateOne({
																						$set: {
																							linkedin: false,
																							c: 1,
																							updated_on: parseInt(Date.now() / 1000)
																						},
																						$addToSet: {
																							searched_by: res.lid
																						},
																						$inc: {
																							n: 1
																						}
																					});
																					cb();
																				}
																			} else if (data.result.indexOf("Invalid HTTP Request") > -1) {
																				invalid_request += 1;
																				batch_confident.find({
																					_id: data._id
																				}).updateOne({
																					$set: {
																						result: "invalid_request",
																						c: 1,
																						updated_on: parseInt(Date.now() / 1000)
																					},
																					$addToSet: {
																						searched_by: res.lid
																					},
																					$inc: {
																						n: 1
																					}
																				});
																				cb();
																			} else {
																				var result;
																				try {
																					result = JSON.parse(data.result);
																					console.log(result["siteStandardProfileRequest"]["url"] + "  ** ", data.lid);
																					var bool2 = result["siteStandardProfileRequest"] && result["siteStandardProfileRequest"]["url"].includes(data["lid"]);
																					var bool = result["siteStandardProfileRequest"]["url"].includes(data.lid.substring(0, 7));
																					if (bool2 || bool) {
																						// validCounter.findAndModify({
																						// 	"_id": "matchedP"
																						// }, {}, {
																						// 	$inc: {
																						// 		seq: 1
																						// 	}
																						// }, {
																						// 	new: true,
																						// 	upsert: true
																						// });
																						predictions2.update({
																							_id: data.lid
																						}, {
																							$set: {
																								"matched": 1
																							}
																						});
																						matchedProfiles.insertOne({
																							_id: data.lid,
																							profile: result,
																							mail: data["_id"],
																							cmpPno: data.index || null
																						}, function(err, ok) {
																							if (err) {
																								console.log(err);
																							} else {
																								console.log("matched_profile");
																								statsInc(overallStats, {
																									"desWiseStats.Matched": 1
																								}, "overall");
																								statsInc(dailyStats, {
																									"desWiseStats.Matched": 1
																								}, "daily");
																							}
																						})
																					}
																				} catch (errr) {
																					console.log("-----------------------&&& ", data.result);
																				}
																				var setDocument = {
																					result: result,
																					pCheck: true,
																					c: 1,
																					updated_on: parseInt(Date.now() / 1000)
																				};
																				try {
																					if (result.distance === 1) {
																						distance_1 += 1;
																						linkedin_true += 1;
																						setDocument["linkedin"] = true;
																					} else if (result.distance === 2) {
																						distance_2 += 1;
																						linkedin_true += 1;
																						setDocument["linkedin"] = true;
																					} else if (result.distance === 3) {
																						distance_3 += 1;
																						linkedin_true += 1;
																						setDocument["linkedin"] = true;
																					} else if (result.distance === -1) {
																						distance_minus1 += 1;
																						linkedin_true += 1;
																						setDocument["linkedin"] = true;
																					} else if (result.distance === 0) {
																						distance_0 += 1;
																						linkedin_true += 1;
																						setDocument["linkedin"] = true;
																					} else if (result.id != null) {
																						no_distance += 1;
																						linkedin_true += 1;
																						setDocument["linkedin"] = true;
																					} else {
																						other_result += 1;
																						setDocument["other_result"] = true;
																					}
																				} catch (errrrr) {
																					console.log(result);
																					console.log(item);
																					process.exit(0);
																				}
																				batch_confident.find({
																					_id: data._id
																				}).updateOne({
																					$set: setDocument,
																					$addToSet: {
																						searched_by: res.lid
																					},
																					$inc: {
																						n: 1
																					}
																				});
																				cb();
																			}
																		}

																	});
																}, function(err) {
																	if (err) {
																		console.error(err.stack);
																		process.exit(0);
																	} else {
																		count++;
																		console.log("cone_counter :" + cone_counter);
																		testPassed(res.token, function(pass) {
																			if (pass) {
																				console.log("test passed!");
																				async.parallel([
																					function(callback) {
																						if (batch_confident.length > 0) {
																							batch_confident.execute(function(err, result) {
																								if (err) {
																									callback(err)
																								} else {
																									console.log("updated successfully!-->", JSON.stringify(result));
																									callback(null, "ok")
																								}
																							});
																						} else {
																							callback(null, "ok")
																						}
																					},
																					function(callback) {
																						if (batch_remaining.length > 0) {
																							batch_remaining.execute(function(err, result) {
																								if (err) {
																									callback(err)
																								} else {
																									console.log("updated successfully!");
																									callback(null, "ok")
																								}
																							});
																						} else {
																							callback(null, "ok")
																						}
																					}
																				], function(err, results) {
																					if (err) {
																						console.error(err.stack);
																						process.exit(0);
																					} else {
																						console.log(res.username, count * BULK_COUNT, no_member, hold, no_access, distance_1 + distance_2 + distance_3);
																						if (throttleReached === 0 && token_expired === 0 && count < MAX_COUNT) {
																							setTimeout(bulk, 0);
																						} else {
																							if (throttleReached > 0) {
																								console.log("Throttle reached in the middle - " + count * BULK_COUNT + " : " + throttleReached);
																							}
																							if (token_expired > 0) {
																								console.log("Token expired in the middle - " + count * BULK_COUNT + " : " + token_expired);
																							}
																							throttle_reached(res, count * BULK_COUNT, throttleReached, token_expired, internal_error, no_access, invalid_request, no_member, linkedin_true, hold, distance_1, distance_2, distance_3, true, distance_0, distance_minus1, no_distance, other_result, false);
																						}
																					}
																				});
																			} else {
																				console.log("test not passed!");
																				if (batch_confident.length > 0) {
																					batch_confident.execute(function(err, result) {
																						if (err) {
																							console.error(err.stack);
																							process.exit(0);
																						} else {
																							console.log(res.username, count * BULK_COUNT, no_member, hold, no_access, distance_1 + distance_2 + distance_3);
																							if (throttleReached > 0) {
																								console.log("Throttle reached in the middle - " + count * BULK_COUNT + " : " + throttleReached);
																							}
																							if (token_expired > 0) {
																								console.log("Token expired in the middle - " + count * BULK_COUNT + " : " + token_expired);
																							}
																							throttle_reached(res, count * BULK_COUNT, throttleReached, token_expired, internal_error, no_access, invalid_request, no_member, linkedin_true, hold, distance_1, distance_2, distance_3, true, distance_0, distance_minus1, no_distance, other_result, false);
																						}
																					});
																				} else {
																					console.log(res.username, count * BULK_COUNT, no_member, hold, no_access, distance_1 + distance_2 + distance_3);
																					if (throttleReached > 0) {
																						console.log("Throttle reached in the middle - " + count * BULK_COUNT + " : " + throttleReached);
																					}
																					if (token_expired > 0) {
																						console.log("Token expired in the middle - " + count * BULK_COUNT + " : " + token_expired);
																					}
																					throttle_reached(res, count * BULK_COUNT, throttleReached, token_expired, internal_error, no_access, invalid_request, no_member, linkedin_true, hold, distance_1, distance_2, distance_3, true, distance_0, distance_minus1, no_distance, other_result, false);
																				}
																			}
																		});
																	}
																});
															}
														});
													}

													var throttleReached = 0;
													var internal_error = 0;
													var token_expired = 0;
													var no_member = 0;
													var hold = 0;
													var linkedin_true = 0;
													var no_access = 0;
													var invalid_request = 0;
													var distance_1 = 0;
													var distance_2 = 0;
													var distance_3 = 0;
													var distance_minus1 = 0;
													var distance_0 = 0;
													var no_distance = 0;
													var other_result = 0;
													var count = 0;
													bulk();
												}
											}
										});
									}
								});
							} else {
								console.log("no tokens!");
								// setTimeout(repeat, 1000);
								setTimeout(function() {
									process.exit(0);
								}, 1000);
							}
						});

					}

					var unique_attempts;

					setTimeout(repeat, 0);
					// setTimeout(function () {
					//     process.exit(0);
					// }, 0);
					function linkedin_crawler(data, token, callback) {
						headers['oauth_token'] = token;

						//url: 'https://api.linkedin.com/v1/people/email=' + data._id + ':(first-name,last-name,headline,location,distance,positions,twitter-accounts,im-accounts,phone-numbers,member-url-resources,picture-urls::(original),site-standard-profile-request,public-profile-url,relation-to-viewer:(connections:(person:(first-name,last-name,headline,site-standard-profile-request,picture-urls::(original)))))',
						var options;
						try {
							// var email_id = aes.decrypt(data._id);
							var email_id = data._id.replace(/%/g, '');
							// console.log(email_id);
							options = {
								url: 'https://api.linkedin.com/v1/people/email=' + email_id + ':(id,first-name,last-name,maiden-name,formatted-name,phonetic-first-name,phonetic-last-name,formatted-phonetic-name,headline,picture-url,industry,honors-awards,summary,specialties,positions,educations,associations,interests,num-recommenders,num-connections,num-connections-capped,last-modified-timestamp,proposal-comments,date-of-birth,publications,patents,languages,skills,certifications,courses,recommendations-received,following,job-bookmarks,suggestions,main-address,three-current-positions,three-past-positions,volunteer,location,distance,twitter-accounts,im-accounts,phone-numbers,member-url-resources,related-profile-views,picture-urls::(original),site-standard-profile-request,public-profile-url,relation-to-viewer:(connections:(person:(first-name,last-name,headline,num-connections,site-standard-profile-request,picture-urls::(original)))))',
								headers: headers
							};
						} catch (errr) {
							console.info(data._id);
							console.error(errr.stack);
							process.exit(0);
						}
						request(options, function(error, response, body) {
							if (error) {
								console.log('request error: ' + error);
								callback(error);
							} else {
								data.result = body.toString();
								// console.log(data.result);
								callback(null, data);
							}
						});
					}


				});
			}
		});
	});
} catch (errrrr) {
	console.error(errrrr.stack);
	process.exit(0)
}

function getMHD(day) {
	var currentOffset = day.getTimezoneOffset();
	var ISTOffset = 330; // IST offset UTC +5:30 
	var ISTTime = new Date(day.getTime() + (ISTOffset + currentOffset) * 60000);
	// ISTTime now represents the time in IST coordinates

	var hh = ISTTime.getHours();
	var mm = ISTTime.getMinutes();
	var dd = ISTTime.getDate();
	var MM = ISTTime.getMonth();
	var yyyy = ISTTime.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (MM < 10) {
		MM = '0' + MM;
	}
	day = new Date(yyyy, MM, dd).getTime() / 1000;
	hour = new Date(yyyy, MM, dd, hh).getTime() / 1000;
	minute = new Date(yyyy, MM, dd, hh, mm).getTime() / 1000;
	return {
		m: minute,
		h: hour,
		d: day
	}
}