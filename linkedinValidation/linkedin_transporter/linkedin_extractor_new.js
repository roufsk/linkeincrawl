var settings = require('./config.json');
var MongoClient = require('mongodb').MongoClient;

function createCollection(cb) {
	MongoClient.connect(settings.linkedinDBURL, function(err, db) {
		dataCollection = db.collection("data1");
		dataCollection2 = db.collection("data2");
		validCounter = db.collection("counters");
		matchedProfiles = db.collection("matched_profiles");
		console.log("funCalls");
		cb();
	})
}
createCollection(function() {
	funCall();
});

function funCall() {
	console.log("funCall");
	matchCount(dataCollection);
	// matchCount(dataCollection2);
}

function matchCount(collection) {
	collection.findAndModify({
		"result": {
			$exists: true
		},
		"pCheck": null
	}, {}, {
		$set: {
			"pCheck": 1
		}
	}, function(err, currItem) {
		currItem = currItem.value;
		console.log(currItem);
		if (currItem != null) {
			var bool2 = currItem["result"]["siteStandardProfileRequest"] && currItem["result"]["siteStandardProfileRequest"]["url"].includes(currItem["lid"]);
			var bool = currItem["result"]["siteStandardProfileRequest"]["url"].includes(currItem.lid.substring(0, 7));
			if (bool || bool2) {
				validCounter.findAndModify({
					"_id": "matchedP"
				}, {}, {
					$inc: {
						seq: 1
					}
				}, {
					new: true,
					upsert: true
				});
				matchedProfiles.insertOne({
					_id: currItem.lid,
					profile: currItem.result,
					mail: currItem["_id"]
				}, function(err, ok) {
					console.log("matched_profile");
					funCall();
				})

			} else {
				console.log("not Valid");
				funCall();
			}

		} else {
			funCall();
		}
	});
}