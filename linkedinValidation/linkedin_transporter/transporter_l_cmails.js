/**
 * Created by sys1039 on 30/3/17.
 */
var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
var async = require('async');
var config = require('./config.json');

function start_DB(host, db, username, password, cb) {
    MongoClient.connect("mongodb://" + username + ":" + password + "@" + host + "/" + db, function(err, db_conn) {
        if (err) {
            cb(err);
        } else {
            cb(null, db_conn);
        }
    });
}

process.on('uncaughtException', function(err) {
    if (err) {
        console.log(err);
        process.exit(0);
    }
});

start_DB("209.58.136.13:27017", "rocketreach", "user", "1ZoT4a.6iZ", function(err, gsb) {
    if (err) {
        console.log(err);
        process.exit(0);
    }
    var processes = gsb.collection("processes");
    var data = gsb.collection("predictions2");

    function transport() {
        console.log('transporter started');
        processes.find({
            _id: new RegExp('linkedin_cmails'),
            priority: {
                $ne: null
            },
            active: true
        }).sort({
            priority: -1
        }).toArray(function(err, docs) {
            if (docs) {
                console.log('records found in processes', docs);
                // process.exit(0);
                async.eachSeries(docs, function(doc, cbk) {
                        if (doc) {
                            var served = 0;
                            console.log(doc);
                            var limit = doc.buffer;
                            var host = doc.info.host;
                            var host_db = doc.info.db;
                            var username = doc.info.username;
                            var password = doc.info.password;
                            var host_count;
                            start_DB(host, host_db, username, password, function(err, hostdb) {
                                if (err) {
                                    console.log(err);
                                    process.exit(0);
                                }
                                if (hostdb) {
                                    var process_collection = hostdb.collection(doc.info.collection);
                                    // process_collection.insert({_id:'dnhejdfc@jvc.com'},);
                                    process_collection.count(function(err, count) {
                                        host_count = count;
                                        console.log('host count:' + host_count + ' limit:' + limit);
                                        if (host_count < limit) {
                                            var remaining = limit - host_count;
                                            console.log('remaining:', remaining);
                                            // process.exit(0);
                                            console.log(doc.query);
                                            var duplicate;
                                            var fields = doc.fields;

                                            function copy_paste(cb) {
                                                if (remaining > 0) {
                                                    duplicate = 0;
                                                    async.eachSeries(doc.query, function(q, callback) {
                                                            // callback;
                                                            console.log("coming here", q);
                                                            q = JSON.parse(q);
                                                            var fq = {};
                                                            // fq['p'] = null;
                                                            fq['lock_cm'] = {
                                                                $ne: true
                                                            };
                                                            Object.keys(q).forEach(function(key, value) {
                                                                fq[key] = q[key];
                                                            });
                                                            if (remaining > 0) {
                                                                console.log(fq);
                                                                console.log(fields);
                                                                var query_limit = remaining < 500 ? remaining : 500;
                                                                data.find(fq, fields).limit(query_limit).toArray(function(err, docs) {
                                                                    if (docs) {

                                                                        async.each(docs, function(item, ncb) {
                                                                                if (remaining > 0) {
                                                                                    var localServed = 0;
                                                                                    async.each(Object.keys(item["companyPredictions"]), function(index, cb) {
                                                                                                remaining--;
                                                                                                localServed++;
                                                                                                var thisItem = {
                                                                                                    "_id": item["companyPredictions"][index],
                                                                                                    "lid": item["_id"],
                                                                                                    "index": index
                                                                                                }
                                                                                                process_collection.insert(thisItem, function(err, res) {
                                                                                                    console.log(thisItem);
                                                                                                    cb();
                                                                                                });

                                                                                            },
                                                                                            function(err) {
                                                                                                if (!err)
                                                                                                    data.update({
                                                                                                        _id: item._id
                                                                                                    }, {
                                                                                                        $set: {
                                                                                                            lock_cm: true
                                                                                                        }
                                                                                                    }, function(err, ok) {
                                                                                                        if (!err) {
                                                                                                            served += localServed;
                                                                                                            console.log('served', served);
                                                                                                            processes.update({
                                                                                                                _id: doc._id
                                                                                                            }, {
                                                                                                                $inc: {
                                                                                                                    'served': localServed
                                                                                                                }
                                                                                                            }, function(err, ok) {
                                                                                                                if (!err) {
                                                                                                                    ncb();
                                                                                                                } else {
                                                                                                                    console.log("updation error");
                                                                                                                }
                                                                                                            });
                                                                                                            console.log('duplicates:', duplicate);
                                                                                                            console.log('remaining:', remaining);
                                                                                                            console.log('query------------', fq);
                                                                                                            console.log('correctly inserted');
                                                                                                        } else {
                                                                                                            console.log("insertion error");
                                                                                                        }
                                                                                                    });
                                                                                            })
                                                                                        // setTimeout(function() {

                                                                                    // }, 0)
                                                                                } else {
                                                                                    ncb();
                                                                                }
                                                                            },
                                                                            function(err) {
                                                                                if (!err) {
                                                                                    callback();
                                                                                }
                                                                            });
                                                                    } else {
                                                                        callback();
                                                                    }
                                                                });
                                                            } else {
                                                                callback();
                                                            }

                                                        },
                                                        function(err) {
                                                            console.log('got it');
                                                            if (!err) {
                                                                if (duplicate == 0) {
                                                                    console.log('no duplicates');
                                                                    return cb();
                                                                } else {
                                                                    console.log('yes duplicates');
                                                                    setTimeout(
                                                                        function() {
                                                                            copy_paste(function(err, result) {
                                                                                if (!err) {
                                                                                    cb();
                                                                                }
                                                                            });
                                                                        }, 1000);
                                                                }

                                                            } else {
                                                                console.log(err, "<---------");
                                                            }

                                                        });
                                                } else {
                                                    cb();
                                                }

                                            }

                                            if (remaining > 0) {
                                                copy_paste(function(err, result) {
                                                    if (!err) {
                                                        console.log('one array element completed');
                                                        if (remaining > 0) {
                                                            console.log('remaining gt 0');
                                                            // processes.update({_id: 'main'}, {$inc: {'balance.any': remaining}});
                                                            hostdb.close();
                                                            cbk();
                                                        } else {
                                                            console.log('remaining not gt 0');
                                                            hostdb.close();
                                                            cbk();
                                                        }
                                                    }
                                                });
                                            } else {
                                                hostdb.close();
                                                cbk();
                                            }
                                        } else {
                                            hostdb.close();
                                            cbk();
                                        }
                                    });

                                } else {
                                    console.log('db error', err);
                                }
                            });
                        }
                    },
                    function(err) {
                        if (!err) {
                            console.log('completed');
                            setTimeout(transport, 0);
                        } else {

                        }
                    });
            } else {
                console.log('no records found');
            }
        });
        // setTimeout(transport,10000);
    }

    transport();
});